package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.TemperamentRest;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Thialyson on 20/04/17.
 */

@EBean
public class TemperamentService {

    @RootContext
    Context context;

    @RestService
    TemperamentRest temperamentRest;


    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Temperament, Integer> temperamentDao;

    @Bean(CookieService.class)
    CookieService cookieUtils;


    public boolean send(Temperament temperament) {

        boolean toReturn;

        try {
            setCookie();
            temperamentRest.send(temperament);
            toReturn = true;
            saveCookie();
        } catch (RestClientException e) {
            toReturn = false;
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<Temperament> requestAll() throws Exception {

        List<Temperament> toReturn = null;
        try {
            setCookie();
            toReturn = temperamentRest.getAll();
            clearTable();
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    private void setCookie() {
        temperamentRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
    }

    private void saveCookie() {
        cookieUtils.storedCookie(temperamentRest.getCookie(RestUtil.MOBILE_COOKIE));
    }


    public List<Temperament> getAll() {
        try {
            return temperamentDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Temperament getlast() {
        try {
            return temperamentDao.queryBuilder().orderBy("id", false).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public Temperament getById(Integer id) {
        try {
            return temperamentDao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void persist(Temperament temperament) throws SQLException {
        if (temperament == null)
            return;
        temperamentDao.createOrUpdate(temperament);
    }


    public void persist(List<Temperament> temperaments) throws SQLException {
        if (temperaments == null)
            return;
        for (Temperament temperament : temperaments) {
            try {
                persist(temperament);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(temperamentDao.getConnectionSource(), Temperament.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
