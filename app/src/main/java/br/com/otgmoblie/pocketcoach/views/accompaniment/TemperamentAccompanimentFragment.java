package br.com.otgmoblie.pocketcoach.views.accompaniment;


import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import br.com.otgmoblie.pocketcoach.views.home.HomeFragment_;
import br.com.otgmoblie.pocketcoach.views.temperament.TemperamentAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

@EFragment(R.layout.fragment_temperament_accompaniment)
public class TemperamentAccompanimentFragment extends Fragment {

    @ViewById(R.id.lvTemperamentAcc)
    ListView listView;

    @ViewById(R.id.avatarAccomp)
    CircleImageView avatar;

    @ViewById(R.id.nameUserAccomp)
    TextView nameUser;

    @ViewById(R.id.textNoTemperaments)
    TextView textNoTemperaments;

    @ViewById(R.id.frequencyAccomp)
    Spinner spinner;

    @ViewById(R.id.lastResult)
    TextView tvLastResult;

    @Bean
    UserService userService;

    @Bean
    TemperamentService temperamentService;

    private Temperament temperament;
    private User userLogged;


    @AfterViews
    void afterViews() {
        showTextLastResult();
        userLogged = userService.getLocalCurrentUser();
        setTexts();

        if (temperamentService.getlast() != null) {
            showList();
            temperament = temperamentService.getlast();
            setAdapter();
        } else {
            showTextNoTemperaments();
        }
    }

    void showTextNoTemperaments() {
        listView.setVisibility(View.GONE);
        textNoTemperaments.setVisibility(View.VISIBLE);
    }

    void showList() {
        textNoTemperaments.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
    }

    void showTextLastResult(){
        spinner.setVisibility(View.GONE);
        tvLastResult.setVisibility(View.VISIBLE);
        tvLastResult.setText(R.string.last_temperament);
    }

    void setTexts() {
        if (userLogged != null) {
            nameUser.setText(userLogged.getName());
//            will.setText(userLogged.getExperience().getName());
            setAvatar();
        }
    }

    void setAvatar() {
        if (userLogged.getAvatar() != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + userLogged.getAvatar();
                Picasso.with(getActivity()).load(url).fit().centerInside().placeholder(R.drawable.ic_user_blue).into(avatar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void setAdapter() {
        TemperamentAdapter temperamentAdapter = new TemperamentAdapter(
                getDimensionNames(new ArrayList<String>()),
                getDimensionValues(new ArrayList<Integer>()),
                getActivity());

        listView.setAdapter(temperamentAdapter);
        temperamentAdapter.notifyDataSetChanged();
    }

    private List<String> getDimensionNames(List<String> dimensionNames) {
        dimensionNames.add(getString(R.string.will));
        dimensionNames.add(getString(R.string.wish));
        dimensionNames.add(getString(R.string.fury));
        dimensionNames.add(getString(R.string.inhibition));
        dimensionNames.add(getString(R.string.sensibility));
        dimensionNames.add(getString(R.string.maturity));
        dimensionNames.add(getString(R.string.control));
        return dimensionNames;
    }

    private List<Integer> getDimensionValues(List<Integer> dimensionValues) {
        dimensionValues.add(temperament.getWillResult());
        dimensionValues.add(temperament.getWishResult());
        dimensionValues.add(temperament.getFuryResult());
        dimensionValues.add(temperament.getInhibitionResult());
        dimensionValues.add(temperament.getSensibilityResult());
        dimensionValues.add(temperament.getCopingResult());
        dimensionValues.add(temperament.getControlResult());
        return dimensionValues;
    }

    @ItemClick(R.id.lvTemperamentAcc)
    void clickListItem(int position) {
        switch (position) {
            case 0:
                callDialog(R.string.text_will);
                break;
            case 1:
                callDialog(R.string.text_wish);
                break;
            case 2:
                callDialog(R.string.text_fury);
                break;
            case 3:
                callDialog(R.string.text_inhibition);
                break;
            case 4:
                callDialog(R.string.text_sensibility);
                break;
            case 5:
                callDialog(R.string.text_coping);

                break;
            case 6:
                callDialog(R.string.text_control);
                break;
        }
    }

    void callDialog(int resIdMessage) {
        AlertDialog.Builder dialog = DialogUtil.createDialog(getActivity(), getString(resIdMessage));
        dialog = DialogUtil.getPositiveButton(dialog);
        dialog.show();
    }

    @Click(R.id.buttonBackTempAcc)
    void clickBack() {
        getActivity().finish();
    }
}
