package br.com.otgmoblie.pocketcoach.views.temperament.emotional;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_item_i)
public class ItemIFragment extends Fragment {

    @ViewById(R.id.tvAsk)
    TextView tvAsk;

    @ViewById(R.id.btRedDarkEmotional)
    Button buttonRedDark;

    @ViewById(R.id.btRedMediumEmotional)
    Button buttonRedMedium;

    @ViewById(R.id.btNeutralEmotional)
    Button buttonWhite;

    @ViewById(R.id.btBlueDarkEmotional)
    Button buttonBlueDark;

    @ViewById(R.id.btBlueMediumEmotional)
    Button buttonBlueMedium;

    Temperament temperament;

    @Bean
    TemperamentService temperamentService;

    @AfterViews
    void afterViews() {
        temperament = temperamentService.getById(0);
        if (temperament != null) {
            checkValue();
        }
        tvAsk.setText(getString(R.string.ask_item_i));
    }

    void checkValue() {
        if (temperament.getItemI() != null) {

            switch (temperament.getItemJ()) {
                case 1:
                    changeValues(buttonRedDark, 1);
                    break;
                case 2:
                    changeValues(buttonRedMedium, 2);
                    break;
                case 3:
                    changeValues(buttonWhite, 3);
                    break;
                case 4:
                    changeValues(buttonBlueMedium, 4);
                    break;
                case 5:
                    changeValues(buttonBlueDark, 5);
                    break;
            }
        } else {
            changeValues(buttonWhite, 3);
        }
    }

    @Click({R.id.btRedDarkEmotional, R.id.btRedMediumEmotional, R.id.btNeutralEmotional, R.id.btBlueDarkEmotional, R.id.btBlueMediumEmotional})
    void handleButtons(Button clickedButton) {

        switch (clickedButton.getId()) {
            case R.id.btRedDarkEmotional:
                changeValues(buttonRedDark, 1);
                break;
            case R.id.btRedMediumEmotional:
                changeValues(buttonRedMedium, 2);
                break;
            case R.id.btNeutralEmotional:
                changeValues(buttonWhite, 3);
                break;
            case R.id.btBlueMediumEmotional:
                changeValues(buttonBlueMedium, 4);
                break;
            case R.id.btBlueDarkEmotional:
                changeValues(buttonBlueDark, 5);
                break;
        }
    }

    void changeValues(Button button, Integer value) {
        clearButtons();
        button.setText(R.string.checkmark);
        temperament.setItemI(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (temperament.getItemI() != null) {
            saveValue();
            switchFragment(new ItemJFragment_());
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (temperament.getItemI() != null)
            saveValue();

        switchFragment(new ItemHFragment_());
    }

    void saveValue() {
        if (temperament != null) {
            try {
                temperamentService.persist(temperament);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void clearButtons() {
        buttonWhite.setText("");
        buttonRedDark.setText("");
        buttonRedMedium.setText("");
        buttonBlueDark.setText("");
        buttonBlueMedium.setText("");
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.temperament_container, fragment);
        transaction.commit();
    }
}
