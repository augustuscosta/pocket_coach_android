package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.RegistrationRest;
import br.com.otgmoblie.pocketcoach.model.Session;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by augustuscosta on 23/06/16.
 */
@EBean
public class RegistrationService {

    @RestService
    RegistrationRest registrationRest;

    @RootContext
    Context context;

    @Bean
    UserService userService;

    @Bean(CookieService.class)
    CookieService cookieUtils;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Session, Integer> sessionDao;


    public synchronized Boolean createUser(User user, FileSystemResource imageAvatar) {
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        setAvatar(user, data, imageAvatar);
        return create(data);
    }

    public Boolean updateUser(User user, FileSystemResource imageAvatar) {
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        setAvatar(user, data, imageAvatar);
        setData(user, data);

        return update(data, Integer.parseInt(userService.getPrefCurrentUserId()));
    }

    private Boolean create(MultiValueMap<String, Object> data) {
        try {
            setHeaders();
            User toReturn = registrationRest.register(data);
            saveCookie();
            return true;
        } catch (RestClientException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Boolean update(MultiValueMap<String, Object> data, Integer id) {
        try {
            setHeaders();
            if (registrationRest.update(data) != null) {
                saveCookie();
                return true;
            }
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setHeaders() {
        registrationRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
        registrationRest.setHeader("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
    }

    private void saveCookie() {
        cookieUtils.storedCookie(registrationRest.getCookie(RestUtil.MOBILE_COOKIE));
    }

    private void setAvatar(User user, MultiValueMap<String, Object> data, FileSystemResource imageAvatar) {
        if (imageAvatar != null) {
            HttpHeaders imageHeaders = new HttpHeaders();
            imageHeaders.setContentType(MediaType.IMAGE_JPEG);
            HttpEntity<Resource> imageEntity = new HttpEntity(imageAvatar, imageHeaders);

            addValue(data, "user[avatar]", imageEntity);
        }

        setData(user, data);
    }

    private void setData(User user, MultiValueMap<String, Object> data) {
        addValue(data, "user[name]", user.getName());
        addValue(data, "user[email]", user.getEmail());
        addValue(data, "user[password]", user.getPassword());
        addValue(data, "user[date_of_birth]", user.getDateOfBirth());
        addValue(data, "user[child]", user.getChild());
        addValue(data, "user[gender]", user.getGender());
        addValue(data, "user[schooling]", user.getSchooling());
        addValue(data, "user[affective_situation]", user.getAffectiveSituation());
        addValue(data, "user[employment_situation]", user.getEmploymentSituation());
        addValue(data, "user[rent]", user.getRent());
        addValue(data, "user[religion]", user.getReligion());
    }

    private void addValue(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value);
        }
    }
}
