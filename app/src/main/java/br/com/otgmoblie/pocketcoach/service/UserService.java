package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.UserRest;
import br.com.otgmoblie.pocketcoach.model.Experience;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class UserService {

    @RootContext
    Context context;

    @RestService
    UserRest userRest;

    @Bean
    CookieService cookieService;

    @Bean
    ExperienceService experienceService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;


    public List<User> getAll() {

        try {
            return userDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public User getLocalCurrentUser() {

        try {
            return userDao.queryBuilder().where().eq("currentUser", true).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User requestCurrentUser() {

        User toReturn = null;

        try {
            userRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = userRest.getCurrentUser();
            Experience teste = experienceService.getById(toReturn.getExperienceId());
            toReturn.setExperience(teste);
            toReturn.setCurrentUser(true);
            persist(toReturn);
            cookieService.storedCookie(userRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toReturn;
    }

    private void persist(User user) {
        try {
            userDao.createOrUpdate(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User get(Integer id) {
        try {
            return userDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    private void clearTable() {
        try {
            TableUtils.clearTable(userDao.getConnectionSource(), User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setPrefCurrentUserId(String value) {
        SharedPreferences sharedPref = context.getSharedPreferences("CURRENTUSERID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("CURRENTUSERID", value);
        editor.apply();
    }

    public String getPrefCurrentUserId() {
        SharedPreferences sharedPref = context.getSharedPreferences("CURRENTUSERID", Context.MODE_PRIVATE);
        return sharedPref.getString("CURRENTUSERID", "");
    }

}
