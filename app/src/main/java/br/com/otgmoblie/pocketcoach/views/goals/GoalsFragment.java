package br.com.otgmoblie.pocketcoach.views.goals;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.service.GoalService;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import dmax.dialog.SpotsDialog;


@EFragment(R.layout.goals_list)
public class GoalsFragment extends Fragment implements AdapterView.OnItemClickListener {

    @ViewById(R.id.recyclerViewListFragment)
    RecyclerView recyclerViewGoals;

    @ViewById(R.id.noGoals)
    TextView textNoGoals;

    @ViewById(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Bean
    GoalService goalService;

    private SpotsDialog progress;

    private List<Goal> goalList = new ArrayList<>();


    @AfterViews
    void afterViews() {
        if (goalService.getAll() != null && goalService.getAll().size() > 0) {
            getValuesFromDatabase();
        } else {
            loadComponents();
        }

        listenRefresh();
    }


    @Background
    void listenRefresh() {
        if (swipeRefresh != null) {
            swipeRefresh.setColorSchemeResources(R.color.com_facebook_blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {
                    loadComponents();
                    swipeRefresh.setRefreshing(false);
                }
            });
        }
    }

    @Background()
    void loadComponents() {
        startProgress();

        try {
            goalService.request();
            closeProgress();
            getValuesFromDatabase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @UiThread
    void getValuesFromDatabase() {
        if (goalService.getAll() != null) {
            setAdapterRecyclerView();
            checkIfListGoalListIsNull();
        }
    }

    void checkIfListGoalListIsNull() {
        if (goalService.getAll().size() == 0) {
            textNoGoals.setVisibility(View.VISIBLE);
        } else {
            textNoGoals.setVisibility(View.GONE);
        }
    }

    void setAdapterRecyclerView() {
        goalList = goalService.getAll();
        GoalsListAdapter goalListAdapter = new GoalsListAdapter(goalList, getActivity(), this);
        recyclerViewGoals.setAdapter(goalListAdapter);
        recyclerViewGoals.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        goalListAdapter.notifyDataSetChanged();
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

    @Click(R.id.goalNewButton)
    void callGoalActivity() {
        NewGoalActivity_.intent(getActivity()).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Integer idGoalClicked = goalList.get(position).getId();
        GoalActivity_.intent(getActivity())
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .extra("id", idGoalClicked).start();
    }
}

