package br.com.otgmoblie.pocketcoach.views.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.service.ExperienceService;
import br.com.otgmoblie.pocketcoach.service.SessionService;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import br.com.otgmoblie.pocketcoach.views.accompaniment.AccompanimentActivity_;
import br.com.otgmoblie.pocketcoach.views.actions_plan.ActionsPlanFragment_;
import br.com.otgmoblie.pocketcoach.views.goals.GoalsFragment_;
import br.com.otgmoblie.pocketcoach.views.humor.HumorEvaluationFragment_;
import br.com.otgmoblie.pocketcoach.views.login.LoginActivity_;
import br.com.otgmoblie.pocketcoach.views.register.RegisterActivity_;
import br.com.otgmoblie.pocketcoach.views.temperament.TemperamentActivity_;
import br.com.otgmoblie.pocketcoach.views.wheel_of_life.WheelOfLifeActivity_;
import de.hdodenhof.circleimageview.CircleImageView;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @ViewById(R.id.toolbarHome)
    Toolbar toolbar;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;

    @Bean
    TemperamentService temperamentService;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    @Bean
    ExperienceService experienceService;

    private TextView nameUserLogged, willUserLogged;

    private CircleImageView avatar;

    private User userLogged;

    boolean doubleBackToExitPressedOnce = false;


    @AfterViews
    void afterViews() {
        requestCurrentUser();
    }

    @Background
    void requestCurrentUser() {
        try {

            if (experienceService.getAll().size() == 0)
                experienceService.requestAll();

            userService.requestCurrentUser();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        getFromDatabase();
    }

    void getFromDatabase() {
        userLogged = userService.getLocalCurrentUser();
        if (userLogged != null)
            userService.setPrefCurrentUserId(userLogged.getId() + "");
        setNavigationView();
    }

    @UiThread
    void setNavigationView() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        setHeadersComponents(header);
    }

    void setHeadersComponents(View header) {
        if (userLogged != null) {
            switchFragment(new HomeFragment_());
            avatar = (CircleImageView) header.findViewById(R.id.imageUserLogged);
            nameUserLogged = (TextView) header.findViewById(R.id.nameUserLogged);
            willUserLogged = (TextView) header.findViewById(R.id.willUser);
            setMenu(R.menu.menu_drawer);
        }
        setAvatarCurrentUser();
    }

    void setAvatarCurrentUser() {
        if (userLogged != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + userLogged.getAvatar();
                Picasso.with(this).load(url).fit().centerCrop().error(R.drawable.ic_user_blue).into(avatar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void setMenu(Integer resId) {
        if (userLogged != null) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(resId);
            nameUserLogged.setText(userLogged.getName());
            willUserLogged.setText(userLogged.getExperience().getName());
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemHome:
                toolbar.setTitle(R.string.home);
                switchFragment(new HomeFragment_());
                break;
            case R.id.itemActionsPlan:
                toolbar.setTitle(R.string.actions_plan);
                switchFragment(new ActionsPlanFragment_());
                break;
            case R.id.itemObjective:
                toolbar.setTitle(R.string.goals);
                switchFragment(new GoalsFragment_());
                break;
            case R.id.itemHumorEvaluation:
                toolbar.setTitle(R.string.humor_evaluation);
                switchFragment(new HumorEvaluationFragment_());
                break;
            case R.id.itemTemperamentEvaluation:
                temperamentService.clearTable();
                TemperamentActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
                break;
            case R.id.itemWheelOfLife:
                wheelOfLiveService.clearTable();
                WheelOfLifeActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
                break;
            case R.id.itemAccompaniment:
                AccompanimentActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
                break;
            case R.id.itemEdit:
                RegisterActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).extra("key", 1).start();
                break;
            case R.id.itemSignOut:
                dialogSignOut();
                break;
        }
        item.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    @UiThread
    void dialogSignOut() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.do_you_wish_go_out));
        builder = DialogUtil.getNegativeButton(builder).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                signOut();
            }
        });
        builder.show();
    }

    @Background
    void signOut() {
        if (userLogged.getFacebookAccount())
            LoginManager.getInstance().logOut();
        sessionService.signOut();
        callLoginActivity();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(this).start();
        finish();
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

    @Receiver(actions = "otgmobile.com.br.coach.UNAUTHORIZED")
    protected void checkUnauthorizedUser() {
        Toast.makeText(this, R.string.access_denied, Toast.LENGTH_SHORT).show();
        sessionService.clearTables();
        callLoginActivity();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_go_out, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
