package br.com.otgmoblie.pocketcoach.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

/**
 * Created by Thialyson on 08/12/16.
 */


public class RestUtil {

//    https://pocketcoach1.herokuapp.com/ Thialyson
//    http://10.0.2.2:3000/ localhost
//    192.168.100.6.

//    http://192.168.100.5:3000/ acessar servidor local pelo mobile rails s -b ip da maquina + porta

    public static final String ROOT_URL_INDEX = "http://192.168.100.3:3000/";
    public static final String ROOT_URL = "http://192.168.100.3:3000/api/";
    public static final String MOBILE_COOKIE = "_PocketCoash_session";

    public static void loggerForError(RestClientException e){
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM: " + e.getMessage());
        Log.e("ERRO NA CONEXÃO", "Erro ---> CAUSA: " + e.getCause());
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM ESPECIFICA: " + e.getMostSpecificCause().getMessage());
    }

    public static void checkUnauthorized(RestClientException e, Context context){
        e.printStackTrace();
        HttpClientErrorException exception = (HttpClientErrorException) e;
        if(HttpStatus.UNAUTHORIZED.equals(exception.getStatusCode())){
            String action = "otgmobile.com.br.coach.UNAUTHORIZED";
            Intent intent = new Intent();
            intent.setAction(action);
            context.sendBroadcast(intent);
        }
    }
}
