package br.com.otgmoblie.pocketcoach.views.wheel_of_life;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Score;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.admob.AdMobActivity_;
import dmax.dialog.SpotsDialog;
import me.xiaopan.swsv.CircularLayout;
import me.xiaopan.swsv.SpiderWebScoreView;

/**
 * Created by Thialyson on 06/01/17.
 */
@EFragment(R.layout.fragment_result_wheel_of_life)
public class ResultWheelOfLifeFragment extends Fragment {

    @ViewById(R.id.spiderWebScore)
    SpiderWebScoreView spiderWebScoreView;

    @ViewById(R.id.circularLayout)
    CircularLayout circularLayout;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    WheelOfLive wheelOfLive;

    private SpotsDialog progress;

    @AfterViews
    void afterViews() {
        if (wheelOfLiveService.getById(0) != null) {
            wheelOfLive = wheelOfLiveService.getById(0);
            setupRadarChart(spiderWebScoreView, circularLayout, getScores());
        }
    }

    private void setupRadarChart(SpiderWebScoreView spiderWebScoreView, CircularLayout circularLayout, Score... scores) {
        float[] scoreArray = getScoreArray(scores);

        spiderWebScoreView.setScores(10f, scoreArray);
        setScoreViewColors(spiderWebScoreView);
        circularLayout.removeAllViews();

        populateScoreView(circularLayout, scores);
    }

    private Score[] getScores() {

        return new Score[]{
                new Score(wheelOfLive.getHealthAndDisposition()),
                new Score(wheelOfLive.getIntellectualDevelopment()),
                new Score(wheelOfLive.getEmotionalBalance()),
                new Score(wheelOfLive.getCareerAndBusiness()),
                new Score(wheelOfLive.getFinances()),
                new Score(wheelOfLive.getSocialContribution()),
                new Score(wheelOfLive.getFamily()),
                new Score(wheelOfLive.getRelationshipWithLove()),
                new Score(wheelOfLive.getSocialLife()),
                new Score(wheelOfLive.getRecreation()),
                new Score(wheelOfLive.getSpirituality()),
                new Score(wheelOfLive.getSatisfactionAndFullfillment())
        };
    }

    private float[] getScoreArray(Score... scores) {

        float[] scoreArray = new float[scores.length];

        for (int i = 0; i < scores.length; i++) {
            scoreArray[i] = scores[i].getScore();
        }
        return scoreArray;
    }

    @SuppressLint("InlinedApi")
    private void populateScoreView(CircularLayout circularLayout, Score... scores) {

        for (int i = 0; i < scores.length; i++) {

            String[] labels = new String[]{"Saúde", "Desenvolvimeto\nIntelectual", "Equilíbrio\nEmocional", "Carreira\ne Negócio", "Finanças", "Contribuição\nSocial", "Família", "Relacionamento\nAmoroso", "Vida\nSocial", "Lazer", "Espiritualidade", "Satisfação"};

            TextView scoreTextView = (TextView) LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.score, circularLayout, false);
            scoreTextView.setTextSize(9);
            scoreTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            setScoreTextColor(scoreTextView, scores[i].getScore());

            scoreTextView.setText(labels[i] + "\n" + scores[i].getScore());
            circularLayout.addView(scoreTextView);
        }
    }

    @SuppressLint("NewApi")
    private void setScoreTextColor(TextView scoreTextView, float score) {
        if (score < 3.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.colorRedHumor, null));
        } else if (score >= 4 && score < 7.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.yellow_chart, null));
        } else if (score >= 8 && score < 9.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.green_chart, null));
        } else {
            scoreTextView.setTextColor(getResources().getColor(R.color.colorPrimaryDark, null));
        }
    }

    private void setScoreViewColors(SpiderWebScoreView spiderWebScoreView) {
        spiderWebScoreView.setLineWidth(0.5f);
        spiderWebScoreView.setLineColor(Color.BLACK);
        spiderWebScoreView.setScoreStrokeWidth(0.5f);
        spiderWebScoreView.setScoreStrokeColor(Color.BLACK);
    }


    @Click(R.id.btSendWheel)
    void sendWheel() {
        sendWheelOfLife();
    }

    @Background
    void sendWheelOfLife() {
        startProgress();
        wheelOfLive.setId(null);
        if (wheelOfLiveService.send(wheelOfLive)) {
            closeProgress();
            callAdMobActivity();
        }
        closeProgress();
    }

    private void callHomeActivity() {
        AdMobActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        getActivity().finish();
    }

    void callAdMobActivity() {
        AdMobActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        getActivity().finish();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

}
