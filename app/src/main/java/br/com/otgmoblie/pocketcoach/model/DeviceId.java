package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by augustuscosta on 06/07/16.
 */
@JsonRootName("device_id")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceId {

    @JsonProperty
    private String os = "ANDROID";

    @JsonProperty
    private String token;

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
