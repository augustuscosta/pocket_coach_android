package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Geovanna on 08/12/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Session{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty("id")
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String email;

    @DatabaseField
    @JsonProperty
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}