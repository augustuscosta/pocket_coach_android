package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Thialyson on 08/12/16.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Temperament {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private Date date;

    @DatabaseField
    @JsonProperty
    private String result;

    @DatabaseField
    @JsonProperty("will_result")
    private Integer willResult; // 1-8

    @DatabaseField
    @JsonProperty("wish_result")
    private Integer wishResult;  // 9-12

    @DatabaseField
    @JsonProperty("fury_result")
    private Integer furyResult; // 13-20

    @DatabaseField
    @JsonProperty("inhibition_result")
    private Integer inhibitionResult; // 21-28

    @DatabaseField
    @JsonProperty("sensibility_result")
    private Integer sensibilityResult; // 29-36

    @DatabaseField
    @JsonProperty("coping_result")
    private Integer copingResult; //37-44

    @DatabaseField
    @JsonProperty("control_result")
    private Integer controlResult; //45-52

    @DatabaseField
    @JsonProperty
    private Integer optimist;//1

    @DatabaseField
    @JsonProperty
    private Integer easyPleasure;//2

    @DatabaseField
    @JsonProperty
    private Integer animate;//3

    @DatabaseField
    @JsonProperty
    private Integer selfEsteem;//4

    @DatabaseField
    @JsonProperty
    private Integer enthusiastic;//5

    @DatabaseField
    @JsonProperty
    private Integer motivated;//6

    @DatabaseField
    @JsonProperty
    private Integer objective;//7

    @DatabaseField
    @JsonProperty
    private Integer energetic;//8

    @DatabaseField
    @JsonProperty
    private Integer strongImpulse;//9

    @DatabaseField
    @JsonProperty
    private Integer exaggerateILike;//10

    @DatabaseField
    @JsonProperty
    private Integer surrenderingToPleasure;//11

    @DatabaseField
    @JsonProperty
    private Integer crazyIWantSomething;//12

    @DatabaseField
    @JsonProperty
    private Integer immediatist;//13

    @DatabaseField
    @JsonProperty
    private Integer extremist;//14

    @DatabaseField
    @JsonProperty
    private Integer obstinate;//15

    @DatabaseField
    @JsonProperty
    private Integer impatient;//16

    @DatabaseField
    @JsonProperty
    private Integer angry;//17
    @DatabaseField
    @JsonProperty
    private Integer aggressive;//18

    @DatabaseField
    @JsonProperty
    private Integer explosive;//19

    @DatabaseField
    @JsonProperty
    private Integer distrustful;//20

    @DatabaseField
    @JsonProperty
    private Integer audacious;//21

    @DatabaseField
    @JsonProperty
    private Integer spontaneous;//22

    @DatabaseField
    @JsonProperty
    private Integer unpreoccupied;//23

    @DatabaseField
    @JsonProperty
    private Integer reagent;//24

    @DatabaseField
    @JsonProperty
    private Integer careless;//25

    @DatabaseField
    @JsonProperty
    private Integer impulsive;//26

    @DatabaseField
    @JsonProperty
    private Integer imprudent;//27

    @DatabaseField
    @JsonProperty
    private Integer risk;//28

    @DatabaseField
    @JsonProperty
    private Integer guilty;//29

    @DatabaseField
    @JsonProperty
    private Integer rejected;//30

    @DatabaseField
    @JsonProperty
    private Integer criticism;//31

    @DatabaseField
    @JsonProperty
    private Integer hurt;//32

    @DatabaseField
    @JsonProperty
    private Integer trauma;//33

    @DatabaseField
    @JsonProperty
    private Integer stress;//34

    @DatabaseField
    @JsonProperty
    private Integer pressure;//35

    @DatabaseField
    @JsonProperty
    private Integer frustration;//36

    @DatabaseField
    @JsonProperty
    private Integer assumeGuilt;//37

    @DatabaseField
    @JsonProperty
    private Integer toFaceProblem;//38

    @DatabaseField
    @JsonProperty
    private Integer resolveProblem;//39

    @DatabaseField
    @JsonProperty
    private Integer resolveProblemNow;//40

    @DatabaseField
    @JsonProperty
    private Integer resolveConflict;//41

    @DatabaseField
    @JsonProperty
    private Integer findSolution;//42

    @DatabaseField
    @JsonProperty
    private Integer learnMyError;//43

    @DatabaseField
    @JsonProperty
    private Integer painMakeStrong;//44

    @DatabaseField
    @JsonProperty
    private Integer attentive;//45

    @DatabaseField
    @JsonProperty
    private Integer focused;//46

    @DatabaseField
    @JsonProperty
    private Integer planActivity;//47

    @DatabaseField
    @JsonProperty
    private Integer concludesTask;//48

    @DatabaseField
    @JsonProperty
    private Integer organized;//49

    @DatabaseField
    @JsonProperty
    private Integer disciplined;//50

    @DatabaseField
    @JsonProperty
    private Integer responsible;//51

    @DatabaseField
    @JsonProperty
    private Integer perfectionist;//52

    @DatabaseField
    @JsonProperty
    private Integer itemA;

    @DatabaseField
    @JsonProperty
    private Integer itemB;

    @DatabaseField
    @JsonProperty
    private Integer itemC;

    @DatabaseField
    @JsonProperty
    private Integer itemD;

    @DatabaseField
    @JsonProperty
    private Integer itemE;

    @DatabaseField
    @JsonProperty
    private Integer itemF;

    @DatabaseField
    @JsonProperty
    private Integer itemG;

    @DatabaseField
    @JsonProperty
    private Integer itemH;

    @DatabaseField
    @JsonProperty
    private Integer itemI;

    @DatabaseField
    @JsonProperty
    private Integer itemJ;

    @DatabaseField
    @JsonProperty
    private Integer itemK;

    @DatabaseField
    @JsonProperty
    private Integer itemL;

    @DatabaseField
    @JsonProperty
    private String itemLooksMe;

    @DatabaseField
    @JsonProperty
    private Integer advantageHumor;

    @DatabaseField
    @JsonProperty
    private Integer prejudiceHumor;

    @DatabaseField
    @JsonProperty
    private Boolean finished;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = "user_id")
    @JsonProperty
    private User user;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getWillResult() {
        return willResult;
    }

    public void setWillResult(Integer willResult) {
        this.willResult = willResult;
    }

    public Integer getWishResult() {
        return wishResult;
    }

    public void setWishResult(Integer wishResult) {
        this.wishResult = wishResult;
    }

    public Integer getFuryResult() {
        return furyResult;
    }

    public void setFuryResult(Integer furyResult) {
        this.furyResult = furyResult;
    }


    public Integer getInhibitionResult() {
        return inhibitionResult;
    }

    public void setInhibitionResult(Integer inhibitionResult) {
        this.inhibitionResult = inhibitionResult;
    }

    public Integer getSensibilityResult() {
        return sensibilityResult;
    }

    public void setSensibilityResult(Integer sensibilityResult) {
        this.sensibilityResult = sensibilityResult;
    }

    public Integer getCopingResult() {
        return copingResult;
    }

    public void setCopingResult(Integer copingResult) {
        this.copingResult = copingResult;
    }

    public Integer getControlResult() {
        return controlResult;
    }

    public void setControlResult(Integer controlResult) {
        this.controlResult = controlResult;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getOptimist() {
        return optimist;
    }

    public void setOptimist(Integer optimist) {
        this.optimist = optimist;
    }

    public Integer getEasyPleasure() {
        return easyPleasure;
    }

    public void setEasyPleasure(Integer easyPleasure) {
        this.easyPleasure = easyPleasure;
    }

    public Integer getAnimate() {
        return animate;
    }

    public void setAnimate(Integer animate) {
        this.animate = animate;
    }

    public Integer getSelfEsteem() {
        return selfEsteem;
    }

    public void setSelfEsteem(Integer selfEsteem) {
        this.selfEsteem = selfEsteem;
    }

    public Integer getEnthusiastic() {
        return enthusiastic;
    }

    public void setEnthusiastic(Integer enthusiastic) {
        this.enthusiastic = enthusiastic;
    }

    public Integer getMotivated() {
        return motivated;
    }

    public void setMotivated(Integer motivated) {
        this.motivated = motivated;
    }

    public Integer getObjective() {
        return objective;
    }

    public void setObjective(Integer objective) {
        this.objective = objective;
    }

    public Integer getEnergetic() {
        return energetic;
    }

    public Integer getStrongImpulse() {
        return strongImpulse;
    }

    public void setStrongImpulse(Integer strongImpulse) {
        this.strongImpulse = strongImpulse;
    }

    public Integer getExaggerateILike() {
        return exaggerateILike;
    }

    public void setExaggerateILike(Integer exaggerateILike) {
        this.exaggerateILike = exaggerateILike;
    }

    public Integer getSurrenderingToPleasure() {
        return surrenderingToPleasure;
    }

    public void setSurrenderingToPleasure(Integer surrenderingToPleasure) {
        this.surrenderingToPleasure = surrenderingToPleasure;
    }

    public Integer getCrazyIWantSomething() {
        return crazyIWantSomething;
    }

    public void setCrazyIWantSomething(Integer crazyIWantSomething) {
        this.crazyIWantSomething = crazyIWantSomething;
    }

    public void setEnergetic(Integer energetic) {
        this.energetic = energetic;
    }

    public Integer getImmediatist() {
        return immediatist;
    }

    public void setImmediatist(Integer immediatist) {
        this.immediatist = immediatist;
    }

    public Integer getExtremist() {
        return extremist;
    }

    public void setExtremist(Integer extremist) {
        this.extremist = extremist;
    }

    public Integer getObstinate() {
        return obstinate;
    }

    public void setObstinate(Integer obstinate) {
        this.obstinate = obstinate;
    }

    public Integer getImpatient() {
        return impatient;
    }

    public void setImpatient(Integer impatient) {
        this.impatient = impatient;
    }

    public Integer getAngry() {
        return angry;
    }

    public void setAngry(Integer angry) {
        this.angry = angry;
    }

    public Integer getAggressive() {
        return aggressive;
    }

    public void setAggressive(Integer aggressive) {
        this.aggressive = aggressive;
    }

    public Integer getExplosive() {
        return explosive;
    }

    public void setExplosive(Integer explosive) {
        this.explosive = explosive;
    }

    public Integer getDistrustful() {
        return distrustful;
    }

    public void setDistrustful(Integer distrustful) {
        this.distrustful = distrustful;
    }

    public Integer getAudacious() {
        return audacious;
    }

    public void setAudacious(Integer audacious) {
        this.audacious = audacious;
    }

    public Integer getSpontaneous() {
        return spontaneous;
    }

    public void setSpontaneous(Integer spontaneous) {
        this.spontaneous = spontaneous;
    }

    public Integer getUnpreoccupied() {
        return unpreoccupied;
    }

    public void setUnpreoccupied(Integer unpreoccupied) {
        this.unpreoccupied = unpreoccupied;
    }

    public Integer getReagent() {
        return reagent;
    }

    public Integer getCareless() {
        return careless;
    }

    public void setCareless(Integer careless) {
        this.careless = careless;
    }

    public Integer getImpulsive() {
        return impulsive;
    }

    public void setImpulsive(Integer impulsive) {
        this.impulsive = impulsive;
    }

    public Integer getImprudent() {
        return imprudent;
    }

    public void setImprudent(Integer imprudent) {
        this.imprudent = imprudent;
    }

    public Integer getRisk() {
        return risk;
    }

    public void setRisk(Integer risk) {
        this.risk = risk;
    }

    public Integer getGuilty() {
        return guilty;
    }

    public void setGuilty(Integer guilty) {
        this.guilty = guilty;
    }

    public Integer getRejected() {
        return rejected;
    }

    public void setRejected(Integer rejected) {
        this.rejected = rejected;
    }

    public Integer getCriticism() {
        return criticism;
    }

    public void setCriticism(Integer criticism) {
        this.criticism = criticism;
    }

    public Integer getHurt() {
        return hurt;
    }

    public void setHurt(Integer hurt) {
        this.hurt = hurt;
    }

    public Integer getTrauma() {
        return trauma;
    }

    public void setTrauma(Integer trauma) {
        this.trauma = trauma;
    }

    public Integer getStress() {
        return stress;
    }

    public void setStress(Integer stress) {
        this.stress = stress;
    }

    public Integer getPressure() {
        return pressure;
    }

    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Integer getFrustration() {
        return frustration;
    }

    public void setFrustration(Integer frustration) {
        this.frustration = frustration;
    }

    public Integer getAssumeGuilt() {
        return assumeGuilt;
    }

    public void setAssumeGuilt(Integer assumeGuilt) {
        this.assumeGuilt = assumeGuilt;
    }

    public Integer getToFaceProblem() {
        return toFaceProblem;
    }

    public void setToFaceProblem(Integer toFaceProblem) {
        this.toFaceProblem = toFaceProblem;
    }

    public Integer getResolveProblem() {
        return resolveProblem;
    }

    public void setResolveProblem(Integer resolveProblem) {
        this.resolveProblem = resolveProblem;
    }

    public Integer getResolveProblemNow() {
        return resolveProblemNow;
    }

    public void setResolveProblemNow(Integer resolveProblemNow) {
        this.resolveProblemNow = resolveProblemNow;
    }

    public Integer getResolveConflict() {
        return resolveConflict;
    }

    public void setResolveConflict(Integer resolveConflict) {
        this.resolveConflict = resolveConflict;
    }

    public Integer getFindSolution() {
        return findSolution;
    }

    public void setFindSolution(Integer findSolution) {
        this.findSolution = findSolution;
    }

    public Integer getLearnMyError() {
        return learnMyError;
    }

    public void setLearnMyError(Integer learnMyError) {
        this.learnMyError = learnMyError;
    }

    public Integer getPainMakeStrong() {
        return painMakeStrong;
    }

    public void setPainMakeStrong(Integer painMakeStrong) {
        this.painMakeStrong = painMakeStrong;
    }

    public Integer getFocused() {
        return focused;
    }

    public Integer getAttentive() {
        return attentive;
    }

    public void setAttentive(Integer attentive) {
        this.attentive = attentive;
    }

    public void setFocused(Integer focused) {
        this.focused = focused;
    }

    public Integer getPlanActivity() {
        return planActivity;
    }

    public void setPlanActivity(Integer planActivity) {
        this.planActivity = planActivity;
    }

    public Integer getConcludesTask() {
        return concludesTask;
    }

    public void setConcludesTask(Integer concludesTask) {
        this.concludesTask = concludesTask;
    }

    public Integer getOrganized() {
        return organized;
    }

    public void setOrganized(Integer organized) {
        this.organized = organized;
    }

    public Integer getDisciplined() {
        return disciplined;
    }

    public void setDisciplined(Integer disciplined) {
        this.disciplined = disciplined;
    }

    public Integer getResponsible() {
        return responsible;
    }

    public void setResponsible(Integer responsible) {
        this.responsible = responsible;
    }

    public Integer getPerfectionist() {
        return perfectionist;
    }

    public void setPerfectionist(Integer perfectionist) {
        this.perfectionist = perfectionist;
    }

    public void setReagent(Integer reagent) {
        this.reagent = reagent;
    }

    public Integer getItemA() {
        return itemA;
    }

    public void setItemA(Integer itemA) {
        this.itemA = itemA;
    }

    public Integer getItemB() {
        return itemB;
    }

    public void setItemB(Integer itemB) {
        this.itemB = itemB;
    }

    public Integer getItemC() {
        return itemC;
    }

    public void setItemC(Integer itemC) {
        this.itemC = itemC;
    }

    public Integer getItemD() {
        return itemD;
    }

    public void setItemD(Integer itemD) {
        this.itemD = itemD;
    }

    public Integer getItemE() {
        return itemE;
    }

    public void setItemE(Integer itemE) {
        this.itemE = itemE;
    }

    public Integer getItemF() {
        return itemF;
    }

    public void setItemF(Integer itemF) {
        this.itemF = itemF;
    }

    public Integer getItemG() {
        return itemG;
    }

    public void setItemG(Integer itemG) {
        this.itemG = itemG;
    }

    public Integer getItemH() {
        return itemH;
    }

    public void setItemH(Integer itemH) {
        this.itemH = itemH;
    }

    public Integer getItemI() {
        return itemI;
    }

    public void setItemI(Integer itemI) {
        this.itemI = itemI;
    }

    public Integer getItemJ() {
        return itemJ;
    }

    public void setItemJ(Integer itemJ) {
        this.itemJ = itemJ;
    }

    public Integer getItemK() {
        return itemK;
    }

    public void setItemK(Integer itemK) {
        this.itemK = itemK;
    }

    public Integer getItemL() {
        return itemL;
    }

    public void setItemL(Integer itemL) {
        this.itemL = itemL;
    }

    public String getItemLooksMe() {
        return itemLooksMe;
    }

    public void setItemLooksMe(String itemLooksMe) {
        this.itemLooksMe = itemLooksMe;
    }

    public Integer getAdvantageHumor() {
        return advantageHumor;
    }

    public void setAdvantageHumor(Integer advantageHumor) {
        this.advantageHumor = advantageHumor;
    }

    public Integer getPrejudiceHumor() {
        return prejudiceHumor;
    }

    public void setPrejudiceHumor(Integer prejudiceHumor) {
        this.prejudiceHumor = prejudiceHumor;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
