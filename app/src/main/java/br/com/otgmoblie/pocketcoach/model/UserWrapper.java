package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Geovanna on 08/12/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWrapper {

    @JsonProperty
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
