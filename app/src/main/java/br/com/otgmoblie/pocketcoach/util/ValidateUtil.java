package br.com.otgmoblie.pocketcoach.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Pattern;

import static java.lang.Integer.getInteger;


public class ValidateUtil {

    public static boolean isNotNullEmail(EditText editText, String msgError) {

        if (!isValidEmail(editText.getText())) {
            editText.requestFocus();
            editText.setText("");
            editText.setError(msgError);
            return false;

        } else {
            return true;
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean checkConfirmPassword(EditText editText, EditText editText2, String msgError) {
        if (!editText.getText().toString().equals(editText2.getText().toString())) {
            editText2.requestFocus();
            editText2.setError(msgError);
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkIfAllAreTrue(List<Boolean> list) {
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkInterval(EditText editText, Integer min, Integer max, String msgError) {
        if (editText.length() < min || editText.length() > max) {
            editText.requestFocus();
            editText.setError(msgError);
            return false;
        } else {
            return true;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static boolean checkDate(EditText editText, Integer size, String msgError) {
        if (editText.length() == size) {

            try {
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                df.parse(editText.getText().toString().replace("/", "-"));
                return true;
            } catch (ParseException e) {
                editText.requestFocus();
                editText.setError(msgError);
                e.printStackTrace();
                return false;
            }
        } else {
            editText.requestFocus();
            editText.setError(msgError);
            return false;

        }
    }

    public static boolean checkTime(EditText editText, String msgError) {

        if (editText.getText() == null || !isValidTime(editText.getText())) {
            editText.requestFocus();
            editText.setText("");
            editText.setError(msgError);
            return false;

        } else {
            return true;
        }
    }

    private static boolean isValidTime(CharSequence target) {
        final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
        return target != null && Pattern.compile(TIME24HOURS_PATTERN).matcher(target).matches();
    }

    public static boolean checkDay(EditText editText, String msgError) {
        Integer day = Integer.parseInt(editText.getText().toString());

        if (editText.getText() != null && day < 31) {
            return true;
        } else {
            editText.requestFocus();
            editText.setText("");
            editText.setError(msgError);
            return false;
        }
    }

    public static boolean checkIfAllAreFalse(List<Boolean> list, View view) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)) {
                return true;
            }
        }
        Snackbar.make(view, "Marque pelo menos um dia", Snackbar.LENGTH_LONG).show();
        return false;
    }

}
