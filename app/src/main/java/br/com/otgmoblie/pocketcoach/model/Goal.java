package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Created by Thialyson on 08/12/16.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Goal {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String description;

    @DatabaseField
    @JsonProperty("date_realization")
    private String dateRealization;

    @DatabaseField
    @JsonProperty
    private String status;

    @DatabaseField
    @JsonProperty
    private String image;

    @DatabaseField
    @JsonProperty("calculate_status")
    private Integer calculateStatus;

    @DatabaseField
    @JsonProperty
    private Boolean specific;

    @DatabaseField
    @JsonProperty
    private Boolean temporal;

    @DatabaseField
    @JsonProperty
    private Boolean relevant;

    @DatabaseField
    @JsonProperty
    private Boolean reachable;

    @DatabaseField
    @JsonProperty
    private Boolean measurable;

    @DatabaseField
    @JsonProperty
    private Boolean ecological;

    @DatabaseField
    @JsonProperty("user_id")
    private Integer userId;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private User user;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateRealization() {
        return dateRealization;
    }

    public void setDateRealization(String dateRealization) {
        this.dateRealization = dateRealization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCalculateStatus() {
        return calculateStatus;
    }

    public void setCalculateStatus(Integer calculateStatus) {
        this.calculateStatus = calculateStatus;
    }

    public Boolean getSpecific() {
        return specific;
    }

    public void setSpecific(Boolean specific) {
        this.specific = specific;
    }

    public Boolean getTemporal() {
        return temporal;
    }

    public void setTemporal(Boolean temporal) {
        this.temporal = temporal;
    }

    public Boolean getRelevant() {
        return relevant;
    }

    public void setRelevant(Boolean relevant) {
        this.relevant = relevant;
    }

    public Boolean getReachable() {
        return reachable;
    }

    public void setReachable(Boolean reachable) {
        this.reachable = reachable;
    }

    public Boolean getMeasurable() {
        return measurable;
    }

    public void setMeasurable(Boolean measurable) {
        this.measurable = measurable;
    }

    public Boolean getEcological() {
        return ecological;
    }

    public void setEcological(Boolean ecological) {
        this.ecological = ecological;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
