package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.List;

/**
 * Created by Thialyson on 08/12/16.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String description;

    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd")
    @JsonProperty("date_realization")
    private Date dateRealization;

    @DatabaseField
    @JsonProperty
    private String status;

    @DatabaseField
    @JsonProperty("type_frequency")
    private String typeFrequency;

    @DatabaseField
    @JsonProperty("goal_id")
    private Integer goalId;

    @DatabaseField
    @JsonProperty
    private String hour;

    @JsonProperty("week_days")
    private List<String> weekDays;

    @JsonProperty("day_of_month")
    private Integer dayOfMonth;

    @DatabaseField(foreign = true,foreignAutoCreate = true,foreignAutoRefresh = true)
    @JsonProperty
    private Goal goal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateRealization() {
        return dateRealization;
    }

    public void setDateRealization(Date dateRealization) {
        this.dateRealization = dateRealization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypeFrequency() {
        return typeFrequency;
    }

    public void setTypeFrequency(String typeFrequency) {
        this.typeFrequency = typeFrequency;
    }

    public Integer getGoalId() {
        return goalId;
    }

    public void setGoalId(Integer goalId) {
        this.goalId = goalId;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public List<String> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<String> weekDays) {
        this.weekDays = weekDays;
    }

    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }
}
