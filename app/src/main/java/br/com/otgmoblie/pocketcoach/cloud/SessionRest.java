package br.com.otgmoblie.pocketcoach.cloud;


import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Delete;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import br.com.otgmoblie.pocketcoach.model.Session;
import br.com.otgmoblie.pocketcoach.model.UserWrapper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Geovanna on 08/12/16.
 */

@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface SessionRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/sign_in_mobile")
    Session signIn(@Body Session session);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Delete("users/sign_out_mobile")
    void signOut();

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/reset_password")
    void resetPassword(@Body Session session);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/sign_in_mobile_facebook")
    Session signInFacebook(@Body UserWrapper user);

    String getCookie(String name);

    void setCookie(String name, String value);


}