package br.com.otgmoblie.pocketcoach.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import br.com.otgmoblie.pocketcoach.model.Experience;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by thialyson on 30/05/17.
 */
@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface ExperienceRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("/experiences")
    List<Experience> getAll();

    String getCookie(String name);

    void setCookie(String name, String value);
}
