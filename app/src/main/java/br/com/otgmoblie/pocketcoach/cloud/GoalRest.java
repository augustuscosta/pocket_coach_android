package br.com.otgmoblie.pocketcoach.cloud;


import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Delete;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Put;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MultiValueMap;

import java.util.List;

import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Geovanna on 08/12/16.
 */

@Rest(rootUrl = RestUtil.ROOT_URL, converters = {ByteArrayHttpMessageConverter.class, FormHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class, StringHttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface GoalRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("create_goal_image.json")
    Goal create(@Body MultiValueMap<String, Object> data);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("goals.json")
    List<Goal> getAll();

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("goals/{id}.json")
    Goal getGoal(@Path Integer id);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Delete("/delete_goal/{id}.json")
    void deleteGoal(@Path Integer id);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Put("/finalize_goal/{id}.json")
    void finalizeGoal(@Path Integer id);

    String getCookie(String name);

    void setCookie(String name, String value);
}
