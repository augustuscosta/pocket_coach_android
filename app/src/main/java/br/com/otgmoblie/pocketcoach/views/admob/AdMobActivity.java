package br.com.otgmoblie.pocketcoach.views.admob;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.home.HomeActivity_;
import dmax.dialog.SpotsDialog;

@EActivity(R.layout.activity_ad_mob)
public class AdMobActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;
    private SpotsDialog progress;

    @AfterViews
    void afterViews() {
        startProgress();
        mInterstitialAd = newInterstitialAd();
        loadInterstitial();
    }

    private void loadInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        mInterstitialAd.loadAd(adRequest);
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                closeProgress();
                showInterstitial();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                closeProgress();
                showToast(R.string.no_internet);
            }

            @Override
            public void onAdClosed() {
                closeProgress();
                showToast(R.string.send_successfully);
                callHomeActivity();
            }
        });
        return interstitialAd;
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            showToast(R.string.no_internet);
        }
    }

    private void callHomeActivity() {
        HomeActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    void showToast(int resId){
        Toast.makeText(AdMobActivity.this, resId, Toast.LENGTH_SHORT).show();
    }

    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }
}
