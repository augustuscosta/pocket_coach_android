package br.com.otgmoblie.pocketcoach.views.wheel_of_life.relationship;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.views.wheel_of_life.professional.FinancesFragment_;
import br.com.otgmoblie.pocketcoach.views.wheel_of_life.professional.SocialContributionFragment;
import br.com.otgmoblie.pocketcoach.views.wheel_of_life.professional.SocialContributionFragment_;

/**
 * Created by Thialyson on 06/01/17.
 */

@EFragment(R.layout.fragment_family)
public class FamilyFragment extends Fragment {

    @ViewById(R.id.buttonPicker)
    Button btPicker;

    @ViewById(R.id.tvTitle)
    TextView tvTitle;

    @ViewById(R.id.tvSubtitle)
    TextView tvSubtitle;

    @ViewById(R.id.tvTextDescription)
    TextView tvDescription;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    WheelOfLive wheelOfLive;

    int valueReceived;

    @AfterViews()
    void afterViews() {
        setTexts();
        checkOnDatabase();
    }

    void checkOnDatabase() {
        if (wheelOfLiveService.getById(0) != null) {
            wheelOfLive = wheelOfLiveService.getById(0);
            System.out.println();
        } else {
            wheelOfLive = new WheelOfLive();
            wheelOfLive.setId(0);
        }
        checkPicker();
    }

    void setTexts() {
        tvTitle.setText(getString(R.string.relationships));
        tvSubtitle.setText(getString(R.string.family));
        tvDescription.setText(getString(R.string.ask_family_1));
    }

    private void checkPicker() {
        if (wheelOfLive.getFamily() != null)
            btPicker.setText(String.valueOf(wheelOfLive.getFamily()));
        else
            btPicker.setText(String.valueOf(valueReceived));
    }

    @Click(R.id.buttonPicker)
    void clickNumberPicker() {
        AlertDialog.Builder dialog = DialogUtil.createDialog(getActivity(), getString(R.string.choose_a_number))
                .setView(DialogUtil.getNumberPicker(getActivity()))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        valueReceived = DialogUtil.valuePicker;
                        setValues(valueReceived);
                    }
                });
        dialog.show();
    }

    void setValues(Integer value) {
        btPicker.setText(String.valueOf(value));
        wheelOfLive.setFamily(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (wheelOfLive.getFamily() != null) {
            saveValue();
            switchFragment(new RelationshipWithLoveFragment_());
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (wheelOfLive.getFamily() != null)
            saveValue();

        switchFragment(new SocialContributionFragment_());
    }

    void saveValue() {
        wheelOfLiveService.persist(wheelOfLive);
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.wheel_of_life_container, fragment);
        transaction.commit();
    }


}
