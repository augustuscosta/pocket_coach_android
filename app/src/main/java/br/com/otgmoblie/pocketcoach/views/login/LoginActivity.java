package br.com.otgmoblie.pocketcoach.views.login;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Experience;
import br.com.otgmoblie.pocketcoach.model.Session;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.service.DeviceIdService;
import br.com.otgmoblie.pocketcoach.service.ExperienceService;
import br.com.otgmoblie.pocketcoach.service.FacebookService;
import br.com.otgmoblie.pocketcoach.service.FacebookServiceCallback;
import br.com.otgmoblie.pocketcoach.service.SessionService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.util.ValidateUtil;
import br.com.otgmoblie.pocketcoach.views.home.HomeActivity;
import br.com.otgmoblie.pocketcoach.views.home.HomeActivity_;
import br.com.otgmoblie.pocketcoach.views.register.RecoverActivity_;
import br.com.otgmoblie.pocketcoach.views.register.RegisterActivity_;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;


@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity implements FacebookServiceCallback {

    @ViewById(R.id.editTextEmail)
    EditText email;

    @ViewById(R.id.editTextPassword)
    EditText password;

    @ViewById(R.id.logo)
    CircleImageView logo;

    @ViewById(R.id.buttonFacebookLogin)
    CircleImageView buttonFacebook;

    @Bean
    DeviceIdService deviceIdService;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;

    @Bean
    ExperienceService experienceService;


    public FacebookService facebookService;

    private SpotsDialog progress;

    boolean doubleBackToExitPressedOnce = false;


    @AfterViews
    void afterViews() {
//        Picasso.with(this).load(R.drawable.target_estiloso).fit().centerInside().into(logo);
        Picasso.with(this).load(R.drawable.facebook_grey).fit().centerCrop().into(buttonFacebook);

        if (sessionService.getCurrentSession() != null) {
            callHomeActivity();
        } else {
            System.out.println();
        }
    }


    @Click(R.id.buttonLogin)
    void loginButton() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            Makelogin();
        }
    }

    @Background
    void Makelogin() {

        startProgress();

        Session session = new Session();
        session.setEmail(email.getText().toString());
        session.setPassword(password.getText().toString());

        try {
            if (sessionService.signIn(session)) {
                deviceIdService.sendDeviceId();
                closeProgress();
                callHomeActivity();
            } else {
                closeProgress();
                showToast(getString(R.string.error_on_login));
            }

        } catch (ResourceAccessException e) {
            e.printStackTrace();
            showToast(getString(R.string.no_internet));

        } catch (Exception e) {
            e.printStackTrace();
            closeProgress();
            callToastWrongEmailOrPassword();
        }
    }

    @UiThread
    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    @UiThread
    void callToastWrongEmailOrPassword() {
        showToast(getString(R.string.email_or_password_incorrect));
    }

    @Click(R.id.buttonFacebookLogin)
    void facebookButton() {
        facebookService = new FacebookService();
        facebookService.init(this, this);
        facebookService.loginFacebook();
    }

    @Background
    void loginFacebook(User user) {

        startProgress();
        try {
            user.setAccountTypeId(1);
            if (sessionService.signInFacebook(user)) {
                deviceIdService.sendDeviceId();
                callHomeActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        closeProgress();
    }

    @Override
    public void onFacebookLoginSuccess(User user) {
        loginFacebook(user);
    }

    @Override
    public void onFacebookLoginError(String message) {
        Toast.makeText(this, getString(R.string.erro_to_acess_facebook) + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.facebookService.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Click(R.id.buttonGoToRegister)
    void callRegisterActivity() {
        RegisterActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @Click(R.id.textViewForgotPassword)
    void callRecoverActivity() {
        RecoverActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @UiThread
    void callHomeActivity() {
        HomeActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_go_out, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
