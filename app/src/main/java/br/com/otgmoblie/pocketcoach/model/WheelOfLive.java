package br.com.otgmoblie.pocketcoach.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Thialyson on 19/12/16.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class WheelOfLive {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty("total_personal")
    private Float totalPersonal;

    @DatabaseField
    @JsonProperty("total_relationships")
    private Float totalRelationships;

    @DatabaseField
    @JsonProperty("total_professional")
    private Float totalProfessional;

    @DatabaseField
    @JsonProperty("total_qualit_life")
    private Float totalQualityOfLife;

    @DatabaseField
    @JsonProperty("emotional_balance")
    private Integer emotionalBalance;

    @DatabaseField
    @JsonProperty("health_and_disposition")
    private Integer healthAndDisposition;

    @DatabaseField
    @JsonProperty("intellectual_development")
    private Integer intellectualDevelopment;

    @DatabaseField
    @JsonProperty("career_and_business")
    private Integer careerAndBusiness;

    @DatabaseField
    @JsonProperty
    private Integer finances;

    @DatabaseField
    @JsonProperty("social_contribution")
    private Integer socialContribution;

    @DatabaseField
    @JsonProperty("recreation")
    private Integer recreation;

    @DatabaseField
    @JsonProperty("satisfaction_and_fullfillment")
    private Integer satisfactionAndFullfillment;

    @DatabaseField
    @JsonProperty
    private Integer spirituality;

    @DatabaseField
    @JsonProperty
    private Integer family;

    @DatabaseField
    @JsonProperty("relationship_with_love")
    private Integer relationshipWithLove;

    @DatabaseField
    @JsonProperty("social_life")
    private Integer socialLife;


    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = "user_id")
    @JsonProperty
    private User user;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getTotalPersonal() {
        return totalPersonal;
    }

    public void setTotalPersonal(Float totalPersonal) {
        this.totalPersonal = totalPersonal;
    }

    public Float getTotalRelationships() {
        return totalRelationships;
    }

    public void setTotalRelationships(Float totalRelationships) {
        this.totalRelationships = totalRelationships;
    }

    public Float getTotalProfessional() {
        return totalProfessional;
    }

    public void setTotalProfessional(Float totalProfessional) {
        this.totalProfessional = totalProfessional;
    }

    public Float getTotalQualityOfLife() {
        return totalQualityOfLife;
    }

    public void setTotalQualityOfLife(Float totalQualityOfLife) {
        this.totalQualityOfLife = totalQualityOfLife;
    }

    public Integer getEmotionalBalance() {
        return emotionalBalance;
    }

    public void setEmotionalBalance(Integer emotionalBalance) {
        this.emotionalBalance = emotionalBalance;
    }

    public Integer getHealthAndDisposition() {
        return healthAndDisposition;
    }

    public void setHealthAndDisposition(Integer healthAndDisposition) {
        this.healthAndDisposition = healthAndDisposition;
    }

    public Integer getIntellectualDevelopment() {
        return intellectualDevelopment;
    }

    public void setIntellectualDevelopment(Integer intellectualDevelopment) {
        this.intellectualDevelopment = intellectualDevelopment;
    }

    public Integer getCareerAndBusiness() {
        return careerAndBusiness;
    }

    public void setCareerAndBusiness(Integer careerAndBusiness) {
        this.careerAndBusiness = careerAndBusiness;
    }

    public Integer getFinances() {
        return finances;
    }

    public void setFinances(Integer finances) {
        this.finances = finances;
    }

    public Integer getSocialContribution() {
        return socialContribution;
    }

    public void setSocialContribution(Integer socialContribution) {
        this.socialContribution = socialContribution;
    }

    public Integer getRecreation() {
        return recreation;
    }

    public void setRecreation(Integer recreation) {
        this.recreation = recreation;
    }

    public Integer getSatisfactionAndFullfillment() {
        return satisfactionAndFullfillment;
    }

    public void setSatisfactionAndFullfillment(Integer satisfactionAndFullfillment) {
        this.satisfactionAndFullfillment = satisfactionAndFullfillment;
    }

    public Integer getSpirituality() {
        return spirituality;
    }

    public void setSpirituality(Integer spirituality) {
        this.spirituality = spirituality;
    }

    public Integer getFamily() {
        return family;
    }

    public void setFamily(Integer family) {
        this.family = family;
    }

    public Integer getRelationshipWithLove() {
        return relationshipWithLove;
    }

    public void setRelationshipWithLove(Integer relationshipWithLove) {
        this.relationshipWithLove = relationshipWithLove;
    }

    public Integer getSocialLife() {
        return socialLife;
    }

    public void setSocialLife(Integer socialLife) {
        this.socialLife = socialLife;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}