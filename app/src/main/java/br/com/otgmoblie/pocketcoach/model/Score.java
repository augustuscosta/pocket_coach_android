package br.com.otgmoblie.pocketcoach.model;

/**
 * Created by thialyson on 21/06/17.
 */

public class Score {
    float score;
    int iconId;

    public Score(float score) {
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
