package br.com.otgmoblie.pocketcoach.views.goals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.util.DateUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by thialyson on 05/05/17.
 */

public class GoalsGridAdapter extends BaseAdapter {
    private List<Goal> goalList;
    private Context context;

    public GoalsGridAdapter(List<Goal> goalList, Context context) {
        this.goalList = goalList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return goalList.size();
    }

    @Override
    public Object getItem(int position) {
        return goalList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class MyViewHolder {
        ImageView imageGrid;
        TextView titleGrid, myDate, myPercent;

        MyViewHolder(View v) {
            imageGrid = (ImageView) v.findViewById(R.id.imageGoalGrid);
            titleGrid = (TextView) v.findViewById(R.id.titleGoalGrid);
            myDate = (TextView) v.findViewById(R.id.dateGrid);
            myPercent = (TextView) v.findViewById(R.id.percentGrid);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_goal_grid, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyViewHolder) row.getTag();
        }

        Goal goal = goalList.get(position);

        holder.titleGrid.setText(goal.getDescription());
        holder.myDate.setText(DateUtil.transformDate(goal.getDateRealization()));
        holder.myPercent.setText(goal.getCalculateStatus() != null ? String.valueOf(goal.getCalculateStatus()+"%") : "0%");
        Picasso.with(context).load(RestUtil.ROOT_URL_INDEX + goal.getImage())
                .fit()
                .centerCrop()
                .error(R.drawable.target_estiloso)
                .into(holder.imageGrid);

        return row;
    }
}
