package br.com.otgmoblie.pocketcoach.views.register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mvc.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.service.DeviceIdService;
import br.com.otgmoblie.pocketcoach.service.RegistrationService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.util.DateUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import br.com.otgmoblie.pocketcoach.util.ValidateUtil;
import br.com.otgmoblie.pocketcoach.views.home.HomeActivity_;
import br.com.otgmoblie.pocketcoach.views.login.LoginActivity_;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

import static android.widget.AdapterView.OnItemSelectedListener;


@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity implements OnItemSelectedListener {


    @ViewById(R.id.buttonRegister)
    Button buttonRegister;

    @ViewById(R.id.avatar)
    CircleImageView avatar;

    @ViewById(R.id.editTextName)
    EditText name;

    @ViewById(R.id.editTextEmail)
    EditText email;

    @ViewById(R.id.child_array)
    Spinner child;

    @ViewById(R.id.gender_array)
    Spinner gender;

    @ViewById(R.id.schooling_array)
    Spinner schooling;

    @ViewById(R.id.affective_array)
    Spinner affectiveSituation;

    @ViewById(R.id.rentArray)
    Spinner rent;

    @ViewById(R.id.employment_array)
    Spinner employmentSituation;

    @ViewById(R.id.religion_array)
    Spinner religionArray;

    @ViewById(R.id.editTextDateBirth)
    EditText dateBirth;

    @ViewById(R.id.editTextPassword)
    EditText password;

    @ViewById(R.id.editTextConfirmationPassword)
    EditText passwordConfirmation;

    @ViewById(R.id.toolbarRegister)
    Toolbar toolbar;

    @Bean
    UserService userService;

    @Bean
    RegistrationService registrationService;

    @Bean
    DeviceIdService deviceIdService;

    Bitmap avatarBitmap;

    @Extra("key")
    int key;

    private static final int PICK_IMAGE_ID = 234;
    private static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 5;

    private String childChosen, schoolingChosen, affectiveChosen, genderChosen, employmentChosen, religionChosen, rentChosen;

    private SpotsDialog progress;


    @AfterViews
    void afterVies() {

        if (key == 0)
            Picasso.with(this).load(R.drawable.ic_user_blue).fit().into(avatar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        populateLists();
        listenItemSelected();
        setTitleByKey();
    }

    void setTitleByKey() {
        if (key == 1) {
            toolbar.setTitle(R.string.edit_profile);
            buttonRegister.setText(R.string.save);
        }
    }

    void listenItemSelected() {
        child.setOnItemSelectedListener(this);
        affectiveSituation.setOnItemSelectedListener(this);
        employmentSituation.setOnItemSelectedListener(this);
        gender.setOnItemSelectedListener(this);
        religionArray.setOnItemSelectedListener(this);
        schooling.setOnItemSelectedListener(this);
        rent.setOnItemSelectedListener(this);
    }

    @UiThread
    void populateLists() {
        createAdapter(schooling, R.array.schooling_array);
        createAdapter(gender, R.array.gender_array);
        createAdapter(employmentSituation, R.array.employment_array);
        createAdapter(child, R.array.child_array);
        createAdapter(affectiveSituation, R.array.affective_array);
        createAdapter(religionArray, R.array.religion_array);
        createAdapter(rent, R.array.rent_array);

        if (key == 1) {
            setComponents(userService.getLocalCurrentUser());
        }
    }

    void createAdapter(Spinner spinner, int array) {
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, array, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
    }

    private void setComponents(User user) {
        if (user != null) {
            name.setText(user.getName() != null ? user.getName() : "");
            email.setText(user.getEmail() != null ? user.getEmail() : "");
            dateBirth.setText(user.getDateOfBirth() != null ? DateUtil.transformDate(user.getDateOfBirth()) : "");

            if (user.getSchooling() != null)
                schooling.setSelection(getPosition(R.array.schooling_array, user.getSchooling()));

            if (user.getGender() != null)
                gender.setSelection(getPosition(R.array.gender_array, user.getGender()));

            if (user.getEmploymentSituation() != null)
                employmentSituation.setSelection(getPosition(R.array.employment_array, user.getEmploymentSituation()));

            if (user.getChild() != null)
                child.setSelection(getPosition(R.array.child_array, user.getChild()));

            if (user.getAffectiveSituation() != null)
                affectiveSituation.setSelection(getPosition(R.array.affective_array, user.getAffectiveSituation()));

            if (user.getReligion() != null)
                religionArray.setSelection(getPosition(R.array.religion_array, user.getReligion()));

            if (user.getRent() != null)
                rent.setSelection(getPosition(R.array.rent_array, user.getRent()));

            if (user.getAvatar() != null)
                Picasso.with(this).load(RestUtil.ROOT_URL_INDEX + user.getAvatar()).placeholder(R.drawable.ic_user_blue).fit().centerInside().into(avatar);
            else
                Picasso.with(this).load(R.drawable.ic_user_blue).fit().into(avatar);
        }
    }

    private Integer getPosition(Integer resId, String name) {
        return Arrays.asList(getResources().getStringArray(resId)).indexOf(name);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.religion_array:
                religionChosen = parent.getSelectedItem().toString();
                break;
            case R.id.child_array:
                childChosen = parent.getSelectedItem().toString();
                break;
            case R.id.schooling_array:
                schoolingChosen = parent.getSelectedItem().toString();
                break;
            case R.id.affective_array:
                affectiveChosen = parent.getSelectedItem().toString();
                break;
            case R.id.employment_array:
                employmentChosen = parent.getSelectedItem().toString();
                break;
            case R.id.gender_array:
                genderChosen = parent.getSelectedItem().toString();
                break;
            case R.id.rentArray:
                rentChosen = parent.getSelectedItem().toString();
                break;
        }
    }


    @Click(R.id.avatar)
    void checkPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else {
            onPickImage(PICK_IMAGE_ID);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onPickImage(requestCode);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onPickImage(Integer integer) {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this, getString(R.string.choose));
        startActivityForResult(chooseImageIntent, integer);
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_ID:

                if (bitmap != null) {
                    avatar.setBackground(null);
                    avatar.setImageBitmap(bitmap);
                    avatarBitmap = bitmap;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Click(R.id.buttonRegister)
    void registrar() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.checkInterval(name, 1, 45, getString(R.string.invalid_name)));
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));
        checkedList.add(ValidateUtil.checkConfirmPassword(password, passwordConfirmation, getString(R.string.diferent_passwords)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            getImage(avatarBitmap);
        }
    }

    @Background
    void getImage(Bitmap avatarBitmap) {

        User user = returnUser(new User());

        if (avatarBitmap != null) {
            FileSystemResource imageAvatar = new FileSystemResource(persistImage(avatarBitmap, getString(R.string.avatar)));
            registerOrUpdate(user, imageAvatar);
        } else {
            registerOrUpdate(user, null);
        }
    }

    void registerOrUpdate(User user, FileSystemResource avatar) {

        startProgress();

        if (key == 1) {
            update(user, avatar);
        } else {
            create(user, avatar);
        }
    }

    void create(User user, FileSystemResource avatar) {
        if (registrationService.createUser(user, avatar) != null) {
            closeProgress();
            callHomeActivity();
        } else {
            closeProgress();
            toastErrorOnRegister();
        }
    }

    void update(User user, FileSystemResource avatar) {
        if (registrationService.updateUser(user, avatar)) {
            closeProgress();
            showSnackbar();
            callHomeActivity();
        } else {
            closeProgress();
            toastErrorOnRegister();
        }
    }

    @UiThread
    void showSnackbar() {
        Toast.makeText(this, R.string.user_was_updated, Toast.LENGTH_LONG).show();
    }

    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = this.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return imageFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public User returnUser(User user) {
        user.setName(name.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setDateOfBirth(dateBirth.getText() != null ? dateBirth.getText().toString() : "");
        user.setChild(childChosen);
        user.setGender(genderChosen);
        user.setSchooling(schoolingChosen);
        user.setAffectiveSituation(affectiveChosen);
        user.setEmploymentSituation(employmentChosen);
        user.setRent(rentChosen);
        user.setReligion(religionChosen);

        return user;
    }

    void callHomeActivity() {
        if (key == 0)
            deviceIdService.sendDeviceId();

        HomeActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @UiThread
    void toastErrorOnRegister() {
        if (key == 1)
            Toast.makeText(this, R.string.coud_not_update_user, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, R.string.could_not_register, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callLoginActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (key == 1) {
            finish();
        } else {
            callLoginActivity();
        }
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }
}


