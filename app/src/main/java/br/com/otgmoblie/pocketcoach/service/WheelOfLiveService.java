package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.WheelOfLifeRest;
import br.com.otgmoblie.pocketcoach.model.Humor;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by thialyson on 27/04/17.
 */
@EBean
public class WheelOfLiveService {

    @RootContext
    Context context;

    @RestService
    WheelOfLifeRest wheelOfLifeRest;

    @Bean
    CookieService cookieUtils;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<WheelOfLive, Integer> wheelOfLiveDao;


    public boolean send(WheelOfLive wheelOfLive) {

        boolean toReturn;

        try {
            setCookie();
            wheelOfLifeRest.send(wheelOfLive);
            toReturn = true;
            setCookie();
        } catch (RestClientException e) {
            toReturn = false;
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<WheelOfLive> requestAll() throws Exception {

        List<WheelOfLive> toReturn = null;
        try {
            setCookie();
            toReturn = wheelOfLifeRest.getAll();
            clearTable();
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    private void setCookie() {
        wheelOfLifeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
    }

    private void saveCookie() {
        cookieUtils.storedCookie(wheelOfLifeRest.getCookie(RestUtil.MOBILE_COOKIE));
    }

    public List<WheelOfLive> getAll() {

        try {
            return wheelOfLiveDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public WheelOfLive getById(Integer id) {
        try {
            return wheelOfLiveDao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public WheelOfLive getlast() {
        try {
            return wheelOfLiveDao.queryBuilder().orderBy("id", false).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void persist(List<WheelOfLive> wheelOfLives) throws SQLException {
        if (wheelOfLives == null)
            return;
        for (WheelOfLive wheelOfLive : wheelOfLives) {
            persist(wheelOfLive);
        }
    }

    public void persist(WheelOfLive wheelOfLive) {
        try {
            wheelOfLiveDao.createOrUpdate(wheelOfLive);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public WheelOfLive get(Integer id) {
        try {
            return wheelOfLiveDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(wheelOfLiveDao.getConnectionSource(), WheelOfLive.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
