package br.com.otgmoblie.pocketcoach.views.temperament;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.admob.AdMobActivity_;
import br.com.otgmoblie.pocketcoach.views.home.HomeActivity_;
import dmax.dialog.SpotsDialog;

/**
 * A simple {@link Fragment} subclass.
 */
@EActivity(R.layout.activity_temperament_result)
public class TemperamentResultActivity extends AppCompatActivity {

    @ViewById(R.id.toolbarTemperamentResult)
    Toolbar toolbar;

    @ViewById(R.id.lvTemperament)
    ListView listView;

    @ViewById(R.id.askDimension)
    TextView askDimension;

    @Bean
    TemperamentService temperamentService;

    private Temperament temperament;

    private SpotsDialog progress;

    @AfterViews
    void afterViews() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        temperament = temperamentService.getById(0);

        setWillResult();
        setWishResult();
        setFuryResult();
        setInhibitionResult();
        setSensibilityResult();
        setCopingResult();
        setControlResult();

        setAdapter();
    }

    void setWillResult() {
        temperament.setWillResult(
                temperament.getOptimist() +
                        temperament.getEasyPleasure() +
                        temperament.getAnimate() +
                        temperament.getSelfEsteem() +
                        temperament.getEnthusiastic() +
                        temperament.getMotivated() +
                        temperament.getObjective() +
                        temperament.getEnergetic()
        );
    }

    void setWishResult() {
        temperament.setWishResult(
                temperament.getOptimist() +
                        temperament.getStrongImpulse() +
                        temperament.getExaggerateILike() +
                        temperament.getSurrenderingToPleasure() +
                        temperament.getCrazyIWantSomething()
        );
    }

    void setFuryResult() {
        temperament.setFuryResult(
                temperament.getOptimist() +
                        temperament.getImmediatist() +
                        temperament.getExtremist() +
                        temperament.getObstinate() +
                        temperament.getImpatient() +
                        temperament.getAngry() +
                        temperament.getAggressive() +
                        temperament.getExplosive() +
                        temperament.getDistrustful()
        );
    }

    void setInhibitionResult() {
        temperament.setInhibitionResult(
                temperament.getOptimist() +
                        temperament.getAudacious() +
                        temperament.getSpontaneous() +
                        temperament.getUnpreoccupied() +
                        temperament.getReagent() +
                        temperament.getCareless() +
                        temperament.getImpulsive() +
                        temperament.getImprudent() +
                        temperament.getRisk()
        );
    }

    void setSensibilityResult() {
        temperament.setSensibilityResult(
                temperament.getOptimist() +
                        temperament.getGuilty() +
                        temperament.getRejected() +
                        temperament.getCriticism() +
                        temperament.getHurt() +
                        temperament.getTrauma() +
                        temperament.getStress() +
                        temperament.getPressure() +
                        temperament.getFrustration()
        );
    }

    void setCopingResult() {
        temperament.setCopingResult(
                temperament.getOptimist() +
                        temperament.getAssumeGuilt() +
                        temperament.getToFaceProblem() +
                        temperament.getResolveProblem() +
                        temperament.getResolveProblemNow() +
                        temperament.getResolveConflict() +
                        temperament.getFindSolution() +
                        temperament.getLearnMyError() +
                        temperament.getPainMakeStrong()
        );
    }

    void setControlResult() {
        temperament.setControlResult(
                temperament.getOptimist() +
                        temperament.getAttentive() +
                        temperament.getFocused() +
                        temperament.getPlanActivity() +
                        temperament.getConcludesTask() +
                        temperament.getOrganized() +
                        temperament.getDisciplined() +
                        temperament.getResponsible() +
                        temperament.getPerfectionist()
        );

        saveTemperament();
    }

    void saveTemperament() {
        try {
            temperamentService.persist(temperament);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void setAdapter() {
        TemperamentAdapter temperamentAdapter = new TemperamentAdapter(
                getDimensionNames(new ArrayList<String>()),
                getDimensionValues(new ArrayList<Integer>()),
                this);

        listView.setAdapter(temperamentAdapter);
        temperamentAdapter.notifyDataSetChanged();
    }

    private List<String> getDimensionNames(List<String> dimensionNames) {
        dimensionNames.add(getString(R.string.will));
        dimensionNames.add(getString(R.string.wish));
        dimensionNames.add(getString(R.string.fury));
        dimensionNames.add(getString(R.string.inhibition));
        dimensionNames.add(getString(R.string.sensibility));
        dimensionNames.add(getString(R.string.maturity));
        dimensionNames.add(getString(R.string.control));
        return dimensionNames;
    }

    private List<Integer> getDimensionValues(List<Integer> dimensionValues) {
        dimensionValues.add(temperament.getWillResult());
        dimensionValues.add(temperament.getWishResult());
        dimensionValues.add(temperament.getFuryResult());
        dimensionValues.add(temperament.getInhibitionResult());
        dimensionValues.add(temperament.getSensibilityResult());
        dimensionValues.add(temperament.getCopingResult());
        dimensionValues.add(temperament.getControlResult());
        return dimensionValues;
    }

    @ItemClick(R.id.lvTemperament)
    void clickListItem(int position) {
        switch (position) {
            case 0:
                callDialog(R.string.text_will);
                break;
            case 1:
                callDialog(R.string.text_wish);
                break;
            case 2:
                callDialog(R.string.text_fury);
                break;
            case 3:
                callDialog(R.string.text_inhibition);
                break;
            case 4:
                callDialog(R.string.text_sensibility);
                break;
            case 5:
                callDialog(R.string.text_coping);

                break;
            case 6:
                callDialog(R.string.text_control);
                break;
        }
    }

    void callDialog(int resIdMessage) {
        AlertDialog.Builder dialog = DialogUtil.createDialog(this, getString(resIdMessage));
        dialog = DialogUtil.getPositiveButton(dialog);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        callHomeActivity();
    }

    void callHomeActivity() {
        HomeActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    void callAdMobActivity() {
        AdMobActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @Click(R.id.btSendTemperament)
    void clickBtSendTemperament() {
        sendTemperament();
    }

    @Background
    void sendTemperament() {
        startProgress();
        temperament.setId(null);
        if (temperamentService.send(temperament)) {
            closeProgress();
            callAdMobActivity();
        }
        closeProgress();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }


}
