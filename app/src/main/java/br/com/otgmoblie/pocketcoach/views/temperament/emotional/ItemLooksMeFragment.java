package br.com.otgmoblie.pocketcoach.views.temperament.emotional;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.views.temperament.TemperamentActivity_;
import br.com.otgmoblie.pocketcoach.views.temperament.TemperamentResultActivity_;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_item_looks_me)
public class ItemLooksMeFragment extends Fragment {

    @ViewById(R.id.buttonA)
    Button buttonA;

    @ViewById(R.id.buttonB)
    Button buttonB;

    @ViewById(R.id.buttonC)
    Button buttonC;

    @ViewById(R.id.buttonD)
    Button buttonD;

    @ViewById(R.id.buttonE)
    Button buttonE;

    @ViewById(R.id.buttonF)
    Button buttonF;

    @ViewById(R.id.buttonG)
    Button buttonG;

    @ViewById(R.id.buttonH)
    Button buttonH;

    @ViewById(R.id.buttonI)
    Button buttonI;

    @ViewById(R.id.buttonJ)
    Button buttonJ;

    @ViewById(R.id.buttonK)
    Button buttonK;

    @ViewById(R.id.buttonL)
    Button buttonL;

    @ViewById(R.id.btNext)
    Button nextButton;

    @Bean
    TemperamentService temperamentService;

    private Temperament temperament;

    @AfterViews()
    void aftersViews() {
        temperament = temperamentService.getById(0);
        nextButton.setText(R.string.finish);
        setTexFixed(getString(R.string.warning_temperament_3));
        checkValue();
    }

    void setTexFixed(String texFixed) {
        TemperamentActivity_ temperamentActivity = (TemperamentActivity_) getActivity();
        temperamentActivity.tvItemNumber.setVisibility(View.VISIBLE);
        temperamentActivity.setTextFixed(texFixed);
    }

    void checkValue() {
        if (temperament.getItemLooksMe() != null) {
            switch (temperament.getItemLooksMe()) {
                case "A":
                    changeValues(buttonA, getString(R.string.a));
                    break;
                case "B":
                    changeValues(buttonB, getString(R.string.b));
                    break;
                case "C":
                    changeValues(buttonC, getString(R.string.c));
                    break;
                case "D":
                    changeValues(buttonD, getString(R.string.d));
                    break;
                case "E":
                    changeValues(buttonE, getString(R.string.e));
                    break;
                case "F":
                    changeValues(buttonF, getString(R.string.f));
                    break;
                case "G":
                    changeValues(buttonG, getString(R.string.g));
                    break;
                case "H":
                    changeValues(buttonH, getString(R.string.h));
                    break;
                case "I":
                    changeValues(buttonI, getString(R.string.i));
                    break;
                case "J":
                    changeValues(buttonJ, getString(R.string.j));
                    break;
                case "K":
                    changeValues(buttonK, getString(R.string.k));
                    break;
                case "L":
                    changeValues(buttonL, getString(R.string.l));
                    break;
            }
        }
    }

    @Click({R.id.buttonA, R.id.buttonB, R.id.buttonC, R.id.buttonD, R.id.buttonE, R.id.buttonF, R.id.buttonG, R.id.buttonH, R.id.buttonI, R.id.buttonJ, R.id.buttonK, R.id.buttonL})
    void handleButtons(Button button) {
        switch (button.getId()) {
            case R.id.buttonA:
                setDialog(buttonA, R.string.ask_item_a, R.string.a);
                break;
            case R.id.buttonB:
                setDialog(buttonB, R.string.ask_item_b, R.string.b);
                break;
            case R.id.buttonC:
                setDialog(buttonC, R.string.ask_item_c, R.string.c);
                break;
            case R.id.buttonD:
                setDialog(buttonD, R.string.ask_item_d, R.string.d);
                break;
            case R.id.buttonE:
                setDialog(buttonE, R.string.ask_item_e, R.string.e);
                break;
            case R.id.buttonF:
                setDialog(buttonF, R.string.ask_item_f, R.string.f);
                break;
            case R.id.buttonG:
                setDialog(buttonG, R.string.ask_item_g, R.string.g);
                break;
            case R.id.buttonH:
                setDialog(buttonH, R.string.ask_item_h, R.string.h);
                break;
            case R.id.buttonI:
                setDialog(buttonI, R.string.ask_item_i, R.string.i);
                break;
            case R.id.buttonJ:
                setDialog(buttonJ, R.string.ask_item_j, R.string.j);
                break;
            case R.id.buttonK:
                setDialog(buttonK, R.string.ask_item_k, R.string.k);
                break;
            case R.id.buttonL:
                setDialog(buttonL, R.string.ask_item_l, R.string.l);
                break;
        }
    }

    void setDialog(final Button button, int resIdMessage, final int resIdItem) {
        AlertDialog.Builder dialog = DialogUtil.createDialog(getActivity(), getString(resIdMessage));
        dialog = DialogUtil.getNegativeButton(dialog)
                .setPositiveButton(R.string.select, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        changeValues(button, getString(resIdItem));
                    }
                });
        dialog.show();
    }

    void changeValues(Button button, String value) {
        clearButtons();
        button.setText(R.string.checkmark);
        temperament.setItemLooksMe(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (temperament.getItemLooksMe() != null) {
            saveValue();
            TemperamentResultActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (temperament.getItemLooksMe() != null)
            saveValue();

        switchFragment(new ItemKFragment_());
    }

    void saveValue() {
        if (temperament != null) {
            try {
                temperamentService.persist(temperament);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void clearButtons() {
        buttonA.setText(getString(R.string.a));
        buttonB.setText(getString(R.string.b));
        buttonC.setText(getString(R.string.c));
        buttonD.setText(getString(R.string.d));
        buttonE.setText(getString(R.string.e));
        buttonF.setText(getString(R.string.f));
        buttonG.setText(getString(R.string.g));
        buttonH.setText(getString(R.string.h));
        buttonI.setText(getString(R.string.i));
        buttonJ.setText(getString(R.string.j));
        buttonK.setText(getString(R.string.k));
        buttonL.setText(getString(R.string.l));
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.temperament_container, fragment);
        transaction.commit();
    }


}
