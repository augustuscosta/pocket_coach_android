package br.com.otgmoblie.pocketcoach.views.goals;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.util.DateUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;


public class GoalsListAdapter extends RecyclerView.Adapter<GoalsListAdapter.ViewHolder> {

    private List<Goal> goalList;
    private LayoutInflater layoutInflater;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;


    public GoalsListAdapter(List<Goal> goalList, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        if (context != null) {
            this.goalList = goalList;
            this.context = context;
            this.onItemClickListener = onItemClickListener;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_goal, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Goal item = goalList.get(position);

        showDate(holder, item);
        setDescription(holder, item);
        setCalculateStatus(holder, item);
        setImageGoal(holder, item);
    }

    private void showDate(ViewHolder holder, Goal item) {
        if (item.getDateRealization() != null)
        holder.textViewGoalDate.setText(DateUtil.transformDate(item.getDateRealization()));
    }

    private void setImageGoal(ViewHolder holder, Goal item) {
        if (goalList != null) {
            if (item.getImage() != null) {
                Picasso.with(context).load(RestUtil.ROOT_URL_INDEX + item.getImage())
                        .fit()
                        .centerCrop()
                        .error(R.drawable.target_estiloso)
                        .into(holder.imageGoal);
            }
        }
    }

    private void setCalculateStatus(ViewHolder holder, Goal item) {
        if (goalList != null) {
            if (item.getDescription() != null)
                holder.progressGoal.setProgress(item.getCalculateStatus());
        }
    }

    private void setDescription(ViewHolder holder, Goal item) {
        if (goalList != null) {
            if (item.getDescription() != null)
                holder.textGoalDescription.setText(item.getDescription());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return goalList.size() > 0 ? goalList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageGoal;
        TextView textViewGoalDate, textGoalDescription;
        ProgressBar progressGoal;
        CardView cardView;

        ViewHolder(final View itemView) {
            super(itemView);

            imageGoal = (ImageView) itemView.findViewById(R.id.imageGoal);
            textViewGoalDate = (TextView) itemView.findViewById(R.id.textGoalDate);
            textGoalDescription = (TextView) itemView.findViewById(R.id.textGoalDescription);
            progressGoal = (ProgressBar) itemView.findViewById(R.id.progressGoal);
            cardView = (CardView) itemView.findViewById(R.id.cardViewGoalList);

            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }

}
