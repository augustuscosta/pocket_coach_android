package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.GoalRest;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Geovanna on 06/01/17.
 */

@EBean
public class GoalService {

    @RootContext
    Context context;

    @RestService
    GoalRest goalRest;

    @Bean
    UserService userService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Goal, Integer> goalDao;

    @Bean
    CookieService cookieUtils;


    public Goal createGoal(Goal goal, FileSystemResource imageGoal) {
        Goal returnedGoal;

        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        HttpHeaders imageHeaders = new HttpHeaders();
        imageHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        if (imageGoal != null) {
            HttpEntity imageEntity = new HttpEntity(imageGoal, imageHeaders);
            addValue(data, "goal[image]", imageEntity);
        }

        getData(goal, data);

        try {
            goalRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            returnedGoal = goalRest.create(data);
            cookieUtils.storedCookie(goalRest.getCookie(RestUtil.MOBILE_COOKIE));
            return returnedGoal;
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getData(Goal goal, MultiValueMap<String, Object> data) {
        addValue(data, "goal[date_realization]", goal.getDateRealization());
        addValue(data, "goal[description]", goal.getDescription());
        addValue(data, "goal[status]", goal.getStatus());
        addValue(data, "goal[specific]", goal.getSpecific() + "");
        addValue(data, "goal[temporal]", goal.getTemporal() + "");
        addValue(data, "goal[relevant]", goal.getRelevant() + "");
        addValue(data, "goal[reachable]", goal.getReachable() + "");
        addValue(data, "goal[measurable]", goal.getMeasurable() + "");
        addValue(data, "goal[ecological]", goal.getEcological() + "");
        addValue(data, "goal[user_id]", userService.getPrefCurrentUserId());
    }

    private void addValue(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value);
        }
    }

    public List<Goal> getAll() {
        try {
            return goalDao.queryBuilder().orderBy("dateRealization", false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Goal> getAllNotFinished() {
        try {
            return goalDao.queryBuilder().orderBy("dateRealization", false).where().eq("status", "Em andamento").query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Goal> getAllFinished() {
        try {
            return goalDao.queryBuilder().orderBy("dateRealization", false).where().eq("status", "Finalizado").query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Goal> request() throws Exception {

        List<Goal> toReturn = null;

        try {
            goalRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            toReturn = goalRest.getAll();
            persist(toReturn);
            cookieUtils.storedCookie(goalRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

        return toReturn;
    }

    public Boolean finalizaGoal(Integer id) throws Exception {

        try {
            goalRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            goalRest.finalizeGoal(id);
            cookieUtils.storedCookie(goalRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean deleteGoal(Integer id) throws Exception {

        try {
            goalRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            goalRest.deleteGoal(id);
            cookieUtils.storedCookie(goalRest.getCookie(RestUtil.MOBILE_COOKIE));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Goal getGoal(Integer id) throws Exception {

        Goal toReturn = null;
        try {
            goalRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            toReturn = goalRest.getGoal(id);
            persist(toReturn);
            cookieUtils.storedCookie(goalRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public Goal getById(Integer id) {
        try {
            return goalDao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void persist(Goal goal) throws SQLException {
        if (goal == null)
            return;
        goalDao.createOrUpdate(goal);
    }


    private void persist(List<Goal> goals) throws SQLException {
        if (goals == null)
            return;
        for (Goal goal : goals) {
            try {
                persist(goal);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(goalDao.getConnectionSource(), Goal.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}


