package br.com.otgmoblie.pocketcoach.util;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by thialyson on 11/04/17.
 */

public class DateUtil {


    @SuppressLint("SimpleDateFormat")
    public static String transformDate(String dateStr) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
            String teste = new SimpleDateFormat("dd/MM/yyyy").format(date);
            return teste;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatDate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static Date formatString(String dateStr, String hour, int key) {
        String hourCorrect = hour != null ? hour : "00:00:";
        DateFormat dateFormat;
        if (key == 0)
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        else
            dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String datahora = dateStr.replace("/", "-") + " " + hourCorrect + ":00";
        try {
            Date teste = dateFormat.parse(datahora);
            return teste;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


}
