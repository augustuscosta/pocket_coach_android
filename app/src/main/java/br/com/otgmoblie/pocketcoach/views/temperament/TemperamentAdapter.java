package br.com.otgmoblie.pocketcoach.views.temperament;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.otgmoblie.pocketcoach.R;

/**
 * Created by thialyson on 08/06/17.
 */

public class TemperamentAdapter extends BaseAdapter {

    private List<String> dimensionNames;
    private List<Integer> dimensionValues;
    private Context context;

    public TemperamentAdapter(List<String> dimensionNames, List<Integer> dimensionValues, Context context) {
        this.dimensionNames = dimensionNames;
        this.dimensionValues = dimensionValues;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dimensionNames.size();
    }

    @Override
    public Object getItem(int position) {
        return dimensionNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class MyViewHolder {
        ImageView ivDimensionResult;
        TextView tvDimension, tvDimensionResult, askDimension;

        MyViewHolder(View v) {
            ivDimensionResult = (ImageView) v.findViewById(R.id.ivDimensionResult);
            tvDimension = (TextView) v.findViewById(R.id.tvDimension);
            tvDimensionResult = (TextView) v.findViewById(R.id.tvDimensionResult);
            askDimension = (TextView) v.findViewById(R.id.askDimension);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_temperament, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyViewHolder) row.getTag();
        }

        String dimenName = dimensionNames.get(position);
        Integer dimenValue = dimensionValues.get(position);

        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/Chunkfive.otf");
        holder.tvDimension.setTypeface(type);

        holder.tvDimension.setText(dimenName);

        setComponentsByPosition(position, dimenValue, holder);

        return row;
    }

    private void setComponentsByPosition(Integer position, Integer dimenValue, MyViewHolder holder) {
        switch (position) {
            case 0:
                loadComponents(dimenValue, holder, 26, 35, 42, 47);
                break;
            case 1:
                loadComponents(dimenValue, holder, 12, 16, 20, 24);
                break;
            case 2:
                loadComponents(dimenValue, holder, 22, 29, 36, 42);
                break;
            case 3:
                loadComponents(dimenValue, holder, 32, 37, 42, 46);
                break;
            case 4:
                loadComponents(dimenValue, holder, 29, 36, 42, 47);
                break;
            case 5:
                loadComponents(dimenValue, holder, 28, 37, 43, 48);
                break;
            case 6:
                loadComponents(dimenValue, holder, 28, 37, 43, 48);
                break;
        }
    }

    private void loadComponents(Integer dimenValue, MyViewHolder holder, Integer value1, Integer value2, Integer value3, Integer value4) {
        if (dimenValue != null) {

            if (dimenValue < value1) {
                loadByVeryLow(holder);
            } else if (dimenValue >= value1 && dimenValue < value2) {
                loadByLow(holder);
            } else if (dimenValue >= value2 && dimenValue < value3) {
                loadByMedium(holder);
            } else if (dimenValue >= value3 && dimenValue < value4) {
                loadByHigh(holder);
            } else {
                loadByVeryHigh(holder);
            }
        }
    }

    private void loadByVeryLow(MyViewHolder holder) {
        setComponents(holder, R.string.very_low_, R.drawable.double_down);
    }

    private void loadByLow(MyViewHolder holder) {
        setComponents(holder, R.string.low_, R.drawable.down);
    }


    private void loadByMedium(MyViewHolder holder) {
        setComponents(holder, R.string.medium_, R.drawable.minus);
    }

    private void loadByHigh(MyViewHolder holder) {
        setComponents(holder, R.string.high_, R.drawable.up);
    }

    private void loadByVeryHigh(MyViewHolder holder) {
        setComponents(holder, R.string.very_high_, R.drawable.double_up);
    }

    private void setComponents(MyViewHolder holder, int resIdMessage, int resIdImage) {
        holder.tvDimensionResult.setText(resIdMessage);
        Picasso.with(context).load(resIdImage).into(holder.ivDimensionResult);
    }

}
