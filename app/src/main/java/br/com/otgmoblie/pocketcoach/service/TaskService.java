package br.com.otgmoblie.pocketcoach.service;

import android.annotation.SuppressLint;
import android.content.Context;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.TaskRest;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Geovanna on 06/01/17.
 */

@EBean
public class TaskService {

    @RootContext
    Context context;

    @RestService
    TaskRest taskRest;


    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Task, Integer> taskDao;

    @Bean(CookieService.class)
    CookieService cookieUtils;


    public List<Task> getAll() {
        try {
            return taskDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @SuppressLint("SimpleDateFormat")
    public List<Task> getAllByDate(Date date) {
        try {
            String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
            List<Task> teste = taskDao.queryBuilder().where().eq("dateRealization", date2).query();
            return teste;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Boolean create(Task task) {

        try {
            setCookie();
            taskRest.create(task);
            saveCookie();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Task> getTasksByGoalId(Integer id) throws Exception {

        List<Task> toReturn;
        try {
            setCookie();
            toReturn = taskRest.getTasksByGoalId(id);
            persist(toReturn);
            saveCookie();

            if (toReturn.size() > 0)
                return toReturn;
            else
                return Collections.emptyList();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
            return Collections.emptyList();
        }

    }

    public Boolean finalizeTask(Integer id) throws Exception {

        try {
            setCookie();
            taskRest.finalizeTask(id);
            saveCookie();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean deleteTask(Integer id) throws Exception {

        try {
            setCookie();
            taskRest.deleteTask(id);
            deleteById(id);
            saveCookie();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setCookie() {
        taskRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
    }

    private void saveCookie() {
        cookieUtils.storedCookie(taskRest.getCookie(RestUtil.MOBILE_COOKIE));
    }


    public List<Task> requestAll() throws Exception {

        List<Task> toReturn = null;
        try {
            setCookie();
            toReturn = taskRest.getAll();
            clearTable();
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<Task> getByGoalId(Integer id) {
        try {
            return taskDao.queryBuilder().where().eq("goalId", id).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Task getById(Integer id) {
        try {
            return taskDao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteById(Integer id) {
        try {
            taskDao.delete(getById(id));
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void persist(Task task) throws SQLException {
        if (task == null)
            return;
        taskDao.createOrUpdate(task);
    }


    public void persist(List<Task> tasks) throws SQLException {
        if (tasks == null)
            return;
        for (Task task : tasks) {
            try {
                persist(task);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(taskDao.getConnectionSource(), Task.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}


