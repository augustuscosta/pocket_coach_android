package br.com.otgmoblie.pocketcoach.views.wheel_of_life.personal;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.views.temperament.affective.AngryFragment_;
import br.com.otgmoblie.pocketcoach.views.temperament.affective.ExplosiveFragment_;

/**
 * Created by Thialyson on 06/01/17.
 */

@EFragment(R.layout.fragment_health_and_disposition)
public class HealthAndDispositionFragment extends Fragment {

    @ViewById(R.id.buttonPicker)
    Button btPicker;

    @ViewById(R.id.tvTitle)
    TextView tvTitle;

    @ViewById(R.id.tvSubtitle)
    TextView tvSubtitle;

    @ViewById(R.id.tvTextDescription)
    TextView tvDescription;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    WheelOfLive wheelOfLive;

    int valueReceived;

    @AfterViews()
    void afterViews() {
        setTexts();
        checkOnDatabase();
        explanationDialog();
    }

    void setTexts() {
        tvTitle.setText(getString(R.string.personal));
        tvSubtitle.setText(getString(R.string.health_and_disposition));
        tvDescription.setText(getString(R.string.ask_health_and_disposition));
    }

    void checkOnDatabase() {
        if (wheelOfLiveService.getById(0) != null) {
            wheelOfLive = wheelOfLiveService.getById(0);
        } else {
            wheelOfLive = new WheelOfLive();
            wheelOfLive.setId(0);
        }
        checkPicker();
    }

    private void explanationDialog() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.explanation_wheel_of_life));
        builder = DialogUtil.getPositiveButton(builder);
        builder.create().show();
    }

    private void checkPicker() {
        if (wheelOfLive.getHealthAndDisposition() != null)
            btPicker.setText(String.valueOf(wheelOfLive.getHealthAndDisposition()));
        else
            btPicker.setText(String.valueOf(valueReceived));
    }

    @Click(R.id.buttonPicker)
    void clickNumberPicker() {
        AlertDialog.Builder dialog = DialogUtil.createDialog(getActivity(), getString(R.string.choose_a_number))
                .setView(DialogUtil.getNumberPicker(getActivity()))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        valueReceived = DialogUtil.valuePicker;
                        setValues(valueReceived);
                    }
                });
        dialog.show();
    }

    void setValues(Integer value) {
        btPicker.setText(String.valueOf(value));
        wheelOfLive.setHealthAndDisposition(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (wheelOfLive.getHealthAndDisposition() != null) {
            saveValue();
            switchFragment(new IntellectualDevelopmentFragment_());
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (wheelOfLive.getHealthAndDisposition() != null)
            saveValue();

        getActivity().onBackPressed();
    }

    void saveValue() {
            wheelOfLiveService.persist(wheelOfLive);
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.wheel_of_life_container, fragment);
        transaction.commit();
    }

}
