package br.com.otgmoblie.pocketcoach.views.goals;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.service.GoalService;
import br.com.otgmoblie.pocketcoach.service.TaskService;
import br.com.otgmoblie.pocketcoach.util.DateUtil;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import br.com.otgmoblie.pocketcoach.views.task.NewTaskActivity_;
import br.com.otgmoblie.pocketcoach.views.task.TasksListAdapter;
import dmax.dialog.SpotsDialog;

@EActivity(R.layout.activity_goal)
public class GoalActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @ViewById(R.id.recyclerViewTasks)
    RecyclerView recyclerViewTasks;

    @ViewById(R.id.toolbarGoal)
    Toolbar toolbar;

    @ViewById(R.id.colapseToolbar)
    CollapsingToolbarLayout colapseToolbar;

    @ViewById(R.id.dateGoal)
    TextView dateGoal;

    @ViewById(R.id.textNoTasks)
    TextView textNoTasks;

    @ViewById(R.id.progressGoal)
    TextView progressGoal;

    @ViewById(R.id.nameGoal)
    TextView nameGoal;

    @Bean
    GoalService goalService;

    @Bean
    TaskService taskService;

    @Extra("id")
    Integer idGoal;

    private Goal currentGoal;

    private List<Task> taskList = new ArrayList<>();

    private ImageView image;

    private SpotsDialog progress;

    TasksListAdapter tasksListAdapter;

    @AfterViews
    void afterViews() {
        image = new ImageView(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requestGoal();
    }

    @Background
    void requestGoal() {
        startProgress();
        try {
            goalService.getGoal(idGoal);
            currentGoal = goalService.getById(idGoal);
            taskService.getTasksByGoalId(currentGoal.getId());
            closeProgress();
            taskList.clear();
            taskList = taskService.getByGoalId(idGoal);
            loadComponents();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    @UiThread
    void loadComponents() {
        if (currentGoal != null) {
            setImageGoal();
            dateGoal.setText(DateUtil.transformDate(currentGoal.getDateRealization()));
            nameGoal.setText(currentGoal.getDescription());
            progressGoal.setText(currentGoal.getCalculateStatus() + "%");
            setImageToolbar();
            setListTasks();
        }
    }

    void setListTasks() {
        if (taskList != null) {
            checkIfListGoalListIsNull();
            setAdapterRecyclerView();
            setImageGoal();
        }
    }

    @UiThread
    void checkIfListGoalListIsNull() {
        if (taskList.size() > 0) {
            recyclerViewTasks.setVisibility(View.VISIBLE);
            textNoTasks.setVisibility(View.GONE);

        } else {
            recyclerViewTasks.setVisibility(View.GONE);
            textNoTasks.setVisibility(View.VISIBLE);
        }
    }

    @UiThread
    void setAdapterRecyclerView() {
        tasksListAdapter = new TasksListAdapter(taskList, this, this);
        recyclerViewTasks.setAdapter(tasksListAdapter);
        recyclerViewTasks.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        tasksListAdapter.notifyDataSetChanged();
    }

    void setImageGoal() {
        if (currentGoal != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + currentGoal.getImage();
                Picasso.with(this).load(url).fit().centerCrop().into(image);
                setImageToolbar();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void setImageToolbar() {
        if (image.getDrawable() != null)
            colapseToolbar.setBackground(image.getDrawable());
    }

    @Click(R.id.fab)
    void clickFab() {
        NewTaskActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .extra("goalId", currentGoal.getId())
                .extra("goalDescription", currentGoal.getDescription())
                .extra("goalDateRealization", currentGoal.getDateRealization())
                .start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Integer idTaskClicked = taskList.get(position).getId();

        if (view.getId() == R.id.excludeTask) {
            dialogAction(R.string.do_you_want_to_exclude_task, R.id.excludeTask, idTaskClicked);
        } else {
            dialogAction(R.string.do_you_want_to_finalize_task, R.id.statusTask, idTaskClicked);
        }
    }

    @UiThread
    void dialogAction(int resIdQuestion, final int resIdViewId, final Integer idTask) {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(resIdQuestion));
        builder = DialogUtil.getNegativeButton(builder).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (resIdViewId) {
                    case R.id.excludeTask:
                        deleteTask(idTask);
                        break;
                    case R.id.statusTask:
                        finalizeTask(idTask);
                        break;
                }
            }
        });
        builder.show();
    }

    @Background
    void finalizeTask(Integer id) {
        startProgress();
        try {
            taskService.finalizeTask(id);
            closeProgress();
            requestGoal();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @Background
    void deleteTask(Integer id) {
        startProgress();
        try {
            taskService.deleteTask(id);
            closeProgress();
            requestGoal();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }


}
