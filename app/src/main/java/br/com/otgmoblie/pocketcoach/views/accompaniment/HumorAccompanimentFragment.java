package br.com.otgmoblie.pocketcoach.views.accompaniment;


import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.models.BarModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Humor;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.service.HumorService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import de.hdodenhof.circleimageview.CircleImageView;

@EFragment(R.layout.fragment_humor_accompaniment)
public class HumorAccompanimentFragment extends Fragment {

    @ViewById(R.id.avatarAccomp)
    CircleImageView avatar;

    @ViewById(R.id.nameUserAccomp)
    TextView nameUser;

    @ViewById(R.id.textNoHumors)
    TextView textNoHumors;

    @ViewById(R.id.barChartHumor)
    BarChart barChart;

    @ViewById(R.id.frequencyAccomp)
    Spinner spinner;

    @ViewById(R.id.lastResult)
    TextView tvLastWheel;

    @Bean
    HumorService humorService;

    @Bean
    UserService userService;

    private User userLogged;

    private List<Humor> humors = new ArrayList<>();

    private int happyValue, confusedValue, tenseValue, angryValue, tiredValue, sadValue;

    @AfterViews
    void afterViews() {

        tvLastWheel.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);

        userLogged = userService.getLocalCurrentUser();
        populateSpinner();
        setTexts();

        humors = humorService.getByDate(Calendar.getInstance().getTime());
        setValues();
    }

    void setTexts() {
        if (userLogged != null) {
            nameUser.setText(userLogged.getName());
            setAvatar();
        }
    }

    void populateSpinner() {
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.frequency_query_array,
                android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
    }

    void setAvatar() {
        if (userLogged.getAvatar() != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + userLogged.getAvatar();
                Picasso.with(getActivity()).load(url).fit().centerInside().placeholder(R.drawable.ic_user_blue).into(avatar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @ItemSelect(R.id.frequencyAccomp)
    void clickSpinner(boolean s, int position) {
        Calendar calendar = Calendar.getInstance();
        clearCalendar(calendar);
        humors.clear();

        switch (position) {
            case 0: // Hoje
                humors = humorService.getByDate(calendar.getTime());
                break;
            case 1://  Semana atual
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                humors = humorService.getByDate(calendar.getTime());
                break;
            case 2://  Mês atual
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                humors = humorService.getByDate(calendar.getTime());
                break;
            case 3://  Ano atual
                calendar.set(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                humors = humorService.getByDate(calendar.getTime());
                break;
        }
        setValues();
    }

    void setValues() {
        if (humors.size() > 0) {
            showChart();
            clearValues();
            for (Humor humor : humors) {
                switch (humor.getName()) {
                    case "Feliz":
                        happyValue += 1;
                        break;
                    case "Confuso":
                        confusedValue += 1;
                        break;
                    case "Tenso":
                        tenseValue += 1;
                        break;
                    case "Irritado":
                        angryValue += 1;
                        break;
                    case "Cansado":
                        tiredValue += 1;
                        break;
                    case "Triste":
                        sadValue += 1;
                        break;
                }
            }
            setBarChart();
        } else {
            showTextNoHumors();
        }
    }

    void showTextNoHumors() {
        barChart.setVisibility(View.GONE);
        textNoHumors.setVisibility(View.VISIBLE);
    }

    void showChart() {
        textNoHumors.setVisibility(View.GONE);
        barChart.setVisibility(View.VISIBLE);
    }

    private void clearValues() {
        sadValue = 0;
        happyValue = 0;
        tenseValue = 0;
        angryValue = 0;
        tiredValue = 0;
        confusedValue = 0;
    }

    @SuppressLint("NewApi")
    void setBarChart() {
        barChart.clearChart();
        barChart.addBar(new BarModel(getString(R.string.happy), happyValue, getResources().getColor(R.color.colorGreenHumor, null)));
        barChart.addBar(new BarModel(getString(R.string.confused), confusedValue, getResources().getColor(R.color.colorYellowHumor, null)));
        barChart.addBar(new BarModel(getString(R.string.tense), tenseValue, getResources().getColor(R.color.colorOrangeHumor, null)));
        barChart.addBar(new BarModel(getString(R.string.angry), angryValue, getResources().getColor(R.color.colorRedHumor, null)));
        barChart.addBar(new BarModel(getString(R.string.tired), tiredValue, getResources().getColor(R.color.colorPurpleHumor, null)));
        barChart.addBar(new BarModel(getString(R.string.sad), sadValue, getResources().getColor(R.color.colorCyanHumor, null)));

        barChart.startAnimation();
    }

    void clearCalendar(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
    }

    @Click(R.id.buttonBack)
    void clickBack() {
        getActivity().finish();
    }
}
