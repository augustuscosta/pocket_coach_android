package br.com.otgmoblie.pocketcoach.views.temperament.emotional;


import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.views.temperament.TemperamentActivity_;
import br.com.otgmoblie.pocketcoach.views.temperament.affective.PerfectionistFragment_;

@EFragment(R.layout.fragment_item_a)
public class ItemAFragment extends Fragment {

    @ViewById(R.id.tvAsk)
    TextView tvAsk;

    @ViewById(R.id.btRedDarkEmotional)
    Button buttonRedDark;

    @ViewById(R.id.btRedMediumEmotional)
    Button buttonRedMedium;

    @ViewById(R.id.btNeutralEmotional)
    Button buttonWhite;

    @ViewById(R.id.btBlueDarkEmotional)
    Button buttonBlueDark;

    @ViewById(R.id.btBlueMediumEmotional)
    Button buttonBlueMedium;

    @Bean
    TemperamentService temperamentService;

    TemperamentActivity_ temperamentActivity;

    Temperament temperament;

    @AfterViews
    void afterViews() {
        tvAsk.setText(getString(R.string.ask_item_a));
        setTexFixed(getString(R.string.warning_temperament_2));
        checkIfHasValue();
        explainDialog();
    }

    void setTexFixed(String texFixed) {
        temperamentActivity = (TemperamentActivity_) getActivity();
        temperamentActivity.setTextFixed(texFixed);
        temperamentActivity.setTextFixed(texFixed);
    }

    @UiThread
    void checkIfHasValue() {
        temperament = temperamentService.getById(0);
        if (temperament != null) {
            checkValue();
        }
    }

    private void explainDialog() {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.warning_asks_emotional));
        builder = DialogUtil.getPositiveButton(builder);
        builder.create().show();
    }

    void checkValue() {
        if (temperament.getItemA() != null) {
            switch (temperament.getItemA()) {
                case 1:
                    changeValues(buttonRedDark, 1);
                    break;
                case 2:
                    changeValues(buttonRedMedium, 2);
                    break;
                case 3:
                    changeValues(buttonWhite, 3);
                    break;
                case 4:
                    changeValues(buttonBlueMedium, 4);
                    break;
                case 5:
                    changeValues(buttonBlueDark, 5);
                    break;
            }
        } else {
            changeValues(buttonWhite, 3);
        }
    }

    @Click({R.id.btRedDarkEmotional, R.id.btRedMediumEmotional, R.id.btNeutralEmotional, R.id.btBlueDarkEmotional, R.id.btBlueMediumEmotional})
    void handleButtons(Button clickedButton) {

        switch (clickedButton.getId()) {
            case R.id.btRedDarkEmotional:
                changeValues(buttonRedDark, 1);
                break;
            case R.id.btRedMediumEmotional:
                changeValues(buttonRedMedium, 2);
                break;
            case R.id.btNeutralEmotional:
                changeValues(buttonWhite, 3);
                break;
            case R.id.btBlueMediumEmotional:
                changeValues(buttonBlueMedium, 4);
                break;
            case R.id.btBlueDarkEmotional:
                changeValues(buttonBlueDark, 5);
                break;
        }
    }

    void changeValues(Button button, Integer value) {
        clearButtons();
        button.setText(R.string.checkmark);
        temperament.setItemA(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (temperament.getItemA() != null) {
            saveValue();
            switchFragment(new ItemBFragment_());
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (temperament.getItemA() != null)
            saveValue();

        switchFragment(new PerfectionistFragment_());
    }

    void saveValue() {
        if (temperament != null) {
            try {
                temperamentService.persist(temperament);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void clearButtons() {
        buttonWhite.setText("");
        buttonRedDark.setText("");
        buttonRedMedium.setText("");
        buttonBlueDark.setText("");
        buttonBlueMedium.setText("");
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.temperament_container, fragment);
        transaction.commit();
    }
}
