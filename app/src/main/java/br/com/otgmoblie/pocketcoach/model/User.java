package br.com.otgmoblie.pocketcoach.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * Created by Geovanna on 08/12/16.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class User {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("date_of_birth")
    private String dateOfBirth;

    @DatabaseField
    @JsonProperty
    private String email;

    @DatabaseField
    @JsonProperty
    private String password;

    @DatabaseField
    @JsonProperty
    private String avatar;

    @DatabaseField
    @JsonProperty("current_user")
    private Boolean currentUser;

    @DatabaseField
    @JsonProperty("facebook_account")
    private Boolean facebookAccount;

    @DatabaseField
    @JsonProperty("facebook_token")
    private String facebookToken;

    @DatabaseField
    @JsonProperty
    private String schooling;

    @DatabaseField
    @JsonProperty
    private String child;

    @DatabaseField
    @JsonProperty
    private String gender;

    @DatabaseField
    @JsonProperty
    private String rent;

    @DatabaseField
    @JsonProperty
    private String religion;

    @DatabaseField
    @JsonProperty("affective_situation")
    private String affectiveSituation;

    @DatabaseField
    @JsonProperty("employment_situation")
    private String employmentSituation;

    @DatabaseField
    @JsonProperty("account_type_id")
    private Integer accountTypeId;

    @DatabaseField
    @JsonProperty("experience_id")
    private Integer experienceId;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private Experience experience;

    @ForeignCollectionField()
    @JsonProperty("wheels")
    private Collection<WheelOfLive> wheelOfLives;

    @ForeignCollectionField()
    @JsonProperty("humor")
    private Collection<Humor> humors;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Goal> goals;

    @ForeignCollectionField()
    @JsonProperty
    private Collection<Temperament> temperaments;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Boolean currentUser) {
        this.currentUser = currentUser;
    }

    public Boolean getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(Boolean facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getSchooling() {
        return schooling;
    }

    public void setSchooling(String schooling) {
        this.schooling = schooling;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getAffectiveSituation() {
        return affectiveSituation;
    }

    public void setAffectiveSituation(String affectiveSituation) {
        this.affectiveSituation = affectiveSituation;
    }

    public String getEmploymentSituation() {
        return employmentSituation;
    }

    public void setEmploymentSituation(String employmentSituation) {
        this.employmentSituation = employmentSituation;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public Collection<WheelOfLive> getWheelOfLives() {
        return wheelOfLives;
    }

    public void setWheelOfLives(Collection<WheelOfLive> wheelOfLives) {
        this.wheelOfLives = wheelOfLives;
    }

    public Collection<Humor> getHumors() {
        return humors;
    }

    public void setHumors(Collection<Humor> humors) {
        this.humors = humors;
    }

    public Collection<Goal> getGoals() {
        return goals;
    }

    public void setGoals(Collection<Goal> goals) {
        this.goals = goals;
    }

    public Collection<Temperament> getTemperaments() {
        return temperaments;
    }

    public void setTemperaments(Collection<Temperament> temperaments) {
        this.temperaments = temperaments;
    }

    public Integer getExperienceId() {
        return experienceId;
    }

    public void setExperienceId(Integer experienceId) {
        this.experienceId = experienceId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}


