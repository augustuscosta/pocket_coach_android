package br.com.otgmoblie.pocketcoach.views.accompaniment;


import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.service.HumorService;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import dmax.dialog.SpotsDialog;


@EActivity(R.layout.activity_accompaniment)
public class AccompanimentActivity extends AppCompatActivity {

    @ViewById(R.id.toolbarAccompaniment)
    Toolbar toolbar;

    @ViewById(R.id.tabLayout)
    TabLayout tabLayout;

    @ViewById(R.id.viewPager)
    ViewPager viewPager;

    @Bean
    HumorService humorService;

    @Bean
    TemperamentService temperamentService;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    ViewPagerAdapter viewPagerAdapter;

    private SpotsDialog progress;


    @AfterViews
    void afterViews() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        requests();
        addFragments();
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    void addFragments() {
        if (viewPagerAdapter == null) {
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragments(new HumorAccompanimentFragment_(), getString(R.string.humor).toUpperCase());
            viewPagerAdapter.addFragments(new WheelOfLifeAccompanimentFragment_(), getString(R.string.wheel_of_life).toUpperCase());
            viewPagerAdapter.addFragments(new TemperamentAccompanimentFragment_(), getString(R.string.temperament).toUpperCase());
        }
    }

    @Background
    void requests() {
        startProgress();
        try {
            humorService.requestAll();
            temperamentService.requestAll();
            wheelOfLiveService.requestAll();
            closeProgress();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
