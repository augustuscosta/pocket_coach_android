package br.com.otgmoblie.pocketcoach.views.goals;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.service.GoalService;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import dmax.dialog.SpotsDialog;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_goals_grid)
public class GoalsGridFragment extends Fragment {

    @ViewById(R.id.gridView)
    GridView gridView;

    @ViewById(R.id.tvNoGoals)
    TextView tvNoGoals;

    @Bean
    GoalService goalService;

    private List<Goal> goalList;

    private Integer keyGrid;

    private SpotsDialog progress;


    @AfterViews
    void afterViews() {
        keyGrid = getArguments().getInt("keyGrid");
        setGoals();
    }

    @Background
    void setGoals() {
        startProgress();
        try {
            goalService.request();
            setGoalListByArgument();
            closeProgress();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    void setGoalListByArgument() {
        if (keyGrid == 0) {
            goalList = goalService.getAll();
        } else {
            goalList = goalService.getAllFinished();
        }
        setAdapter();
    }

    @UiThread
    void setAdapter() {
        if (goalList != null && goalList.size() > 0) {
            GoalsGridAdapter goalsGridAdapter = new GoalsGridAdapter(goalList, getActivity());
            gridView.setAdapter(goalsGridAdapter);
            goalsGridAdapter.notifyDataSetChanged();
        } else {
            tvNoGoals.setVisibility(View.VISIBLE);
            setTextNoGoals();
        }
    }

    void setTextNoGoals() {
        if (keyGrid == 0)
            tvNoGoals.setText(R.string.no_goal_registered);
        else
            tvNoGoals.setText(R.string.no_goal_finished);
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

}
