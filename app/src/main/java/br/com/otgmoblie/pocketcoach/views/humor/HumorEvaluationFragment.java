package br.com.otgmoblie.pocketcoach.views.humor;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Humor;
import br.com.otgmoblie.pocketcoach.service.HumorService;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.wheel_of_life.WheelOfLifeActivity_;
import dmax.dialog.SpotsDialog;

/**
 * Created by Thialyson on 19/12/16.
 */
@EFragment(R.layout.fragment_humor_evaluation)
public class HumorEvaluationFragment extends Fragment {

    @ViewById(R.id.backHumor)
    ImageView backHumor;

    @ViewById(R.id.btHelp)
    Button btHelp;

    @Bean
    HumorService humorService;

    @Bean
    UserService userService;

    private SpotsDialog progress;

    String humorName = "Feliz";

    @AfterViews()
    void aftersViews(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_feliz);
        setDialog(R.string.warning_humor_evaluation);
    }

    @Click(R.id.humorHappy)
    void happyClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_feliz);
        humorName = getResources().getString(R.string.happy);
    }

    @Click(R.id.humorTired)
    void tiredClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_cansado);
        humorName = getResources().getString(R.string.tired);
    }

    @Click(R.id.humorAngry)
    void angryClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_irritado);
        humorName = getResources().getString(R.string.angry);
    }

    @Click(R.id.humorUnhappy)
    void unhappyClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_triste);
        humorName = getResources().getString(R.string.sad);
    }

    @Click(R.id.humorConfused)
    void confusedClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_confuso);
        humorName = getResources().getString(R.string.confused);
    }

    @Click(R.id.humorTense)
    void tenseClick(){
        backHumor.setBackgroundResource(R.drawable.roda_humor_tenso);
        humorName = getResources().getString(R.string.tense);
    }

    @Click(R.id.btHelp)
    void helpClick(){
        setDialog(R.string.help_humor_evaluation);
    }

    @Click(R.id.btSaveHumor)
    void saveClick(){
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(R.string.do_you_want_to_save_humor));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Humor humor = new Humor();
                humor.setName(humorName);
                createHumor(humor);
            }
        });
        builder.show();
    }

    @Background
    void createHumor(Humor humor){
        startProgress();
        try {
            humorService.createHumor(humor);
            showToast(getString(R.string.humor_has_saved));
            closeProgress();
            callWheelOfLifeActivity();
        } catch (Exception e) {
            showToast(getString(R.string.error_on_save_humor));
            closeProgress();
            e.printStackTrace();
        }
    }

    @UiThread
    void showToast(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    void callWheelOfLifeActivity(){
        WheelOfLifeActivity_.intent(getActivity()).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).start();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    void setDialog(int resIdMessage) {
        AlertDialog.Builder dialog = DialogUtil.createDialog(getActivity(), getString(resIdMessage));
        dialog = DialogUtil.getPositiveButton(dialog);
        dialog.show();
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }
}
