package br.com.otgmoblie.pocketcoach.views.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Session;
import br.com.otgmoblie.pocketcoach.service.SessionService;
import br.com.otgmoblie.pocketcoach.views.login.LoginActivity_;

@EActivity(R.layout.activity_recover)
public class RecoverActivity extends AppCompatActivity {

    @ViewById(R.id.editTextEmailRecover)
    EditText email;

    @ViewById(R.id.toolbarRecover)
    Toolbar toolbar;

    @Bean
    SessionService sessionService;

    @AfterViews
    void afterViews(){
        setSupportActionBar(toolbar);
        getSupportFragmentManager();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Click(R.id.buttonRecover)
    void resetPasswordButton() {
        if (email.getText() != null) {
            resetPassword();
        } else {
            email.setError(getString(R.string.invalid_email));
        }
    }

    @Background
    void resetPassword() {
        Session session = new Session();
        session.setEmail(email.getText().toString());
        if (sessionService.resetPassword(session))
            showResetPasswordToast();
        else
            emailNotFound();
    }

    @UiThread
    void showResetPasswordToast() {
        Toast.makeText(this, R.string.msg_to_recover_password, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void emailNotFound() {
        Toast.makeText(this, R.string.email_not_found, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                callLoginActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        callLoginActivity();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }
}
