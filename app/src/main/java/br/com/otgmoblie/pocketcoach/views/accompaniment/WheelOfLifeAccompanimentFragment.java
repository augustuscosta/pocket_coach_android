package br.com.otgmoblie.pocketcoach.views.accompaniment;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Score;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.service.UserService;
import br.com.otgmoblie.pocketcoach.service.WheelOfLiveService;
import br.com.otgmoblie.pocketcoach.util.RestUtil;
import br.com.otgmoblie.pocketcoach.views.home.HomeFragment_;
import de.hdodenhof.circleimageview.CircleImageView;
import me.xiaopan.swsv.CircularLayout;
import me.xiaopan.swsv.SpiderWebScoreView;

@EFragment(R.layout.fragment_wheel_of_life_accompaniment)
public class WheelOfLifeAccompanimentFragment extends Fragment {

    @ViewById(R.id.avatarAccomp)
    CircleImageView avatar;

    @ViewById(R.id.nameUserAccomp)
    TextView nameUser;

    @ViewById(R.id.lastResult)
    TextView tvLastResult;

    @ViewById(R.id.textNoWheelOfLife)
    TextView textNoWheelOfLife;

    @ViewById(R.id.frequencyAccomp)
    Spinner spinner;

    @ViewById(R.id.spiderWebScoreAcc)
    SpiderWebScoreView spiderWebScoreView;

    @ViewById(R.id.circularLayoutAcc)
    CircularLayout circularLayout;

    @Bean
    WheelOfLiveService wheelOfLiveService;

    WheelOfLive wheelOfLive;

    @Bean
    UserService userService;

    private User userLogged;


    @AfterViews
    void afterViews() {

        spinner.setVisibility(View.GONE);
        tvLastResult.setVisibility(View.VISIBLE);
        tvLastResult.setText(R.string.last_wheel_of_life);

        userLogged = userService.getLocalCurrentUser();
        setTextViewsUser();

        if (wheelOfLiveService.getlast() != null) {
            showChart();
            wheelOfLive = wheelOfLiveService.getlast();
            setupRadarChart(spiderWebScoreView, circularLayout, getScores());
        } else {
            showTextNoWheelOfLifes();
        }
    }

    void showTextNoWheelOfLifes() {
        circularLayout.setVisibility(View.GONE);
        spiderWebScoreView.setVisibility(View.GONE);
        textNoWheelOfLife.setVisibility(View.VISIBLE);
    }

    void showChart() {
        textNoWheelOfLife.setVisibility(View.GONE);
        circularLayout.setVisibility(View.VISIBLE);
        spiderWebScoreView.setVisibility(View.VISIBLE);
    }

    private void setupRadarChart(SpiderWebScoreView spiderWebScoreView, CircularLayout circularLayout, Score... scores) {
        float[] scoreArray = getScoreArray(scores);

        spiderWebScoreView.setScores(10f, scoreArray);
        setScoreViewColors(spiderWebScoreView);
        circularLayout.removeAllViews();

        populateScoreView(circularLayout, scores);
    }

    private Score[] getScores() {

        return new Score[]{
                new Score(wheelOfLive.getHealthAndDisposition()),
                new Score(wheelOfLive.getIntellectualDevelopment()),
                new Score(wheelOfLive.getEmotionalBalance()),
                new Score(wheelOfLive.getCareerAndBusiness()),
                new Score(wheelOfLive.getFinances()),
                new Score(wheelOfLive.getSocialContribution()),
                new Score(wheelOfLive.getFamily()),
                new Score(wheelOfLive.getRelationshipWithLove()),
                new Score(wheelOfLive.getSocialLife()),
                new Score(wheelOfLive.getRecreation()),
                new Score(wheelOfLive.getSpirituality()),
                new Score(wheelOfLive.getSatisfactionAndFullfillment())
        };
    }

    private float[] getScoreArray(Score... scores) {

        float[] scoreArray = new float[scores.length];

        for (int i = 0; i < scores.length; i++) {
            scoreArray[i] = scores[i].getScore();
        }
        return scoreArray;
    }

    @SuppressLint("InlinedApi")
    private void populateScoreView(CircularLayout circularLayout, Score... scores) {

        for (int i = 0; i < scores.length; i++) {

            String[] labels = new String[]{"Saúde", "Desenvolvimeto\nIntelectual", "Equilíbrio\nEmocional", "Carreira\ne Negócio", "Finanças", "Contribuição\nSocial", "Família", "Relacionamento\nAmoroso", "Vida\nSocial", "Lazer", "Espiritualidade", "Satisfação"};

            TextView scoreTextView = (TextView) LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.score, circularLayout, false);
            scoreTextView.setTextSize(9);
            scoreTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            scoreTextView.setTextAppearance(R.style.ShadowOnTextView);

            setScoreTextColor(scoreTextView, scores[i].getScore());

            scoreTextView.setText(labels[i] + "\n" + scores[i].getScore());
            circularLayout.addView(scoreTextView);
        }
    }

    @SuppressLint("NewApi")
    private void setScoreTextColor(TextView scoreTextView, float score) {
        if (score <= 3.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.colorRedHumor, null));
        } else if (score >= 4 && score <= 7.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.yellow_chart, null));
        } else if (score >= 8 && score <= 9.9) {
            scoreTextView.setTextColor(getResources().getColor(R.color.green_chart, null));
        } else {
            scoreTextView.setTextColor(getResources().getColor(R.color.colorPrimaryDark, null));
        }
    }

    private void setScoreViewColors(SpiderWebScoreView spiderWebScoreView) {
        spiderWebScoreView.setLineWidth(0.5f);
        spiderWebScoreView.setLineColor(Color.BLACK);
        spiderWebScoreView.setScoreStrokeWidth(0.5f);
        spiderWebScoreView.setScoreStrokeColor(Color.BLACK);
    }


    void setTextViewsUser() {
        if (userLogged != null) {
            nameUser.setText(userLogged.getName());
//            will.setText(userLogged.getExperience().getName());
            setAvatar();
        }
    }

    void setAvatar() {
        if (userLogged.getAvatar() != null) {
            try {
                String url = RestUtil.ROOT_URL_INDEX + userLogged.getAvatar();
                Picasso.with(getActivity()).load(url).fit().centerInside().placeholder(R.drawable.ic_user_blue).into(avatar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Click(R.id.buttonBackAcc)
    void clickBack() {
        getActivity().finish();
    }

}
