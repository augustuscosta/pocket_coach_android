package br.com.otgmoblie.pocketcoach.views.temperament;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.views.temperament.affective.OptimistFragment_;

@EActivity(R.layout.activity_temperamet)
public class TemperamentActivity extends AppCompatActivity {

    @ViewById(R.id.tvItemNumber)
    public TextView tvItemNumber;

    @ViewById(R.id.toolbarTemperament)
    Toolbar toolbar;


    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switchFragment(new OptimistFragment_());
    }

    public void setTextFixed(String text) {
        tvItemNumber.setText(text);
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.temperament_container, fragment);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.if_go_out_you_will_lost_progress));
        builder = DialogUtil.getNegativeButton(builder).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TemperamentActivity.super.onBackPressed();
                finish();
            }
        });
        builder.show();
    }
}
