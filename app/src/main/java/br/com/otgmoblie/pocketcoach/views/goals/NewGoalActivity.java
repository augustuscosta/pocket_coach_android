package br.com.otgmoblie.pocketcoach.views.goals;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.mvc.imagepicker.ImagePicker;
import com.vicmikhailau.maskededittext.MaskedEditText;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.service.GoalService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.util.ValidateUtil;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;


@EActivity(R.layout.activity_new_goal)
public class NewGoalActivity extends AppCompatActivity {

    @ViewById(R.id.relevant)
    Switch relevant;

    @ViewById(R.id.reachable)
    Switch reachable;

    @ViewById(R.id.specific)
    Switch specific;

    @ViewById(R.id.ecological)
    Switch ecological;

    @ViewById(R.id.temporal)
    Switch temporal;

    @ViewById(R.id.measurable)
    Switch measurable;

    @ViewById(R.id.controllable)
    Switch controllable;

    @ViewById(R.id.textGoalDate)
    MaskedEditText dateRealization;

    @ViewById(R.id.textGoalDescription)
    EditText goalDescription;

    @ViewById(R.id.imageGoal)
    CircleImageView goalImage;

    @Bean
    GoalService goalService;

    private static final int PICK_IMAGE_ID = 234;
    private static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 5;

    private SpotsDialog progress;

    private Bitmap goalBitmap;


    @Click(R.id.goalButtonCreate)
    void validateFields() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.checkInterval(goalDescription, 1, 50, getString(R.string.field_required)));
        checkedList.add(ValidateUtil.checkInterval(dateRealization, 10, 10, getString(R.string.field_required)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            setImage();
        }
    }

    void setImage() {
        FileSystemResource image = null;
        if (goalBitmap != null) {
            image = new FileSystemResource(persistImage(goalBitmap, getString(R.string.image)));
        }
        createGoal(image);
    }

    @Background
    void createGoal(FileSystemResource image) {
        if (goalService.createGoal(returnGoal(new Goal()), image) != null) {
            callGoalActivity();
        } else {
            toastErrorOnCreate();
        }
    }

    public Goal returnGoal(Goal goal) {
        goal.setDescription(goalDescription.getText().toString());
        goal.setDateRealization(dateRealization.getText().toString());
        goal.setRelevant(relevant.isChecked());
        goal.setReachable(reachable.isChecked());
        goal.setSpecific(specific.isChecked());
        goal.setEcological(ecological.isChecked());
        goal.setTemporal(temporal.isChecked());
        goal.setMeasurable(measurable.isChecked());
        return goal;
    }


    @Nullable
    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = this.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            return imageFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @CheckedChange({R.id.measurable, R.id.temporal, R.id.ecological, R.id.relevant, R.id.reachable, R.id.specific, R.id.controllable})
    public void onChange(CompoundButton item, boolean isChecked) {
        if (isChecked) {

            switch (item.getId()) {
                case R.id.measurable:
                    dialogExplanation(getString(R.string.measurable_text));
                    break;
                case R.id.temporal:
                    dialogExplanation(getString(R.string.temporal_text));
                    break;
                case R.id.ecological:
                    dialogExplanation(getString(R.string.ecological_text));
                    break;
                case R.id.relevant:
                    dialogExplanation(getString(R.string.relevant_text));
                    break;
                case R.id.reachable:
                    dialogExplanation(getString(R.string.reachable_text));
                    break;
                case R.id.specific:
                    dialogExplanation(getString(R.string.specific_text));
                    break;
                case R.id.controllable:
                    dialogExplanation(getString(R.string.controllable_text));
                    break;
            }
        }
    }

    void dialogExplanation(String message) {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, message);
        builder = DialogUtil.getNegativeButton(builder);
        builder = DialogUtil.getPositiveButton(builder);
        builder.show();
    }


    @Click(R.id.imageGoal)
    void checkPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        } else {
            onPickImage(PICK_IMAGE_ID);
        }
    }

    public void onPickImage(Integer integer) {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this, getString(R.string.choose));
        startActivityForResult(chooseImageIntent, integer);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        setBitmap(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    void setBitmap(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_IMAGE_ID:

                if (bitmap != null) {
                    goalBitmap = bitmap;
                    goalImage.setImageBitmap(bitmap);
                }
                break;
        }
    }


    @UiThread
    void callGoalActivity() {
        NewGoalActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
        finish();
    }

    @UiThread
    void toastErrorOnCreate() {
        Toast.makeText(this, R.string.not_goal_register, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

}



