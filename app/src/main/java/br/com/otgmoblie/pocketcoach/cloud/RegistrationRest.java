package br.com.otgmoblie.pocketcoach.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Put;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;

import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.util.RestUtil;


@Rest(rootUrl = RestUtil.ROOT_URL, converters = {ByteArrayHttpMessageConverter.class, FormHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class, StringHttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface RegistrationRest extends RestClientHeaders {


    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("users/register_mobile_multipart")
    User register(@Body MultiValueMap<String, Object> data);

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Put("/update_mobile_multipart")
    HashMap update(@Body MultiValueMap<String, Object> data);

    void setCookie(String name, String value);

    String getCookie(String name);
}
