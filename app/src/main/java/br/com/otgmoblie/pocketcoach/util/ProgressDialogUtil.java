package br.com.otgmoblie.pocketcoach.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;

import br.com.otgmoblie.pocketcoach.R;
import dmax.dialog.SpotsDialog;


/**
 * Created by Thialyson on 03/08/16.
 */


public class ProgressDialogUtil extends Activity {

    @SuppressLint("NewApi")
    public static SpotsDialog callProgressDialog(Context context) {

        SpotsDialog progress = null;
        if (context != null) {
            progress = new SpotsDialog(context, R.style.ProgressAlertDialogStyle);
            progress.setCancelable(true);
            progress.create();
            progress.show();
        }
        return progress;
    }

    public static void closeProgressDialog(SpotsDialog progress) {
        progress.dismiss();
    }

}
