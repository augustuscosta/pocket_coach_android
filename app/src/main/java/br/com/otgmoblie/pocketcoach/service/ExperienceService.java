package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.ExperienceRest;
import br.com.otgmoblie.pocketcoach.model.Experience;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by thialyson on 30/05/17.
 */
@EBean
public class ExperienceService {

    @RootContext
    Context context;

    @RestService
    ExperienceRest experienceRest;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Experience, Integer> experienceDao;

    @Bean
    CookieService cookieUtils;


    public List<Experience> requestAll() throws Exception {

        List<Experience> toReturn = null;
        try {
            setCookie();
            toReturn = experienceRest.getAll();
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    private void setCookie() {
        experienceRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
    }

    private void saveCookie() {
        cookieUtils.storedCookie(experienceRest.getCookie(RestUtil.MOBILE_COOKIE));
    }

    public List<Experience> getAll() {
        try {
            return experienceDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void persist(List<Experience> experiences) throws SQLException {
        if (experiences == null)
            return;
        for (Experience experience : experiences) {
            try {
                persist(experience);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Experience getById(Integer id) {
        try {
            return experienceDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void persist(Experience experience) throws SQLException {
        if (experience == null)
            return;
        experienceDao.createOrUpdate(experience);
    }
}
