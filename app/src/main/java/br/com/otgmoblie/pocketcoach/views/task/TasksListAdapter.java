package br.com.otgmoblie.pocketcoach.views.task;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.util.DateUtil;

/**
 * Created by thialyson on 03/04/17.
 */

public class TasksListAdapter extends RecyclerView.Adapter<TasksListAdapter.ViewHolder> {

    private List<Task> taskList;
    private LayoutInflater layoutInflater;
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;


    public TasksListAdapter(List<Task> taskList, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        if (context != null) {
            this.context = context;
            this.taskList = taskList;
            this.onItemClickListener = onItemClickListener;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public TasksListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_task, parent, false);
        return new TasksListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TasksListAdapter.ViewHolder holder, int position) {
        if (taskList != null && taskList.size() > 0) {

            Task item = taskList.get(position);

            Picasso.with(context).load(R.drawable.ic_delete).fit().centerInside().into(holder.excludeTask);
            showDate(holder, item);
            setDescription(holder, item);
            setImageStatus(holder, item);
        }
    }


    private void showDate(TasksListAdapter.ViewHolder holder, Task item) {
        holder.dateTask.setText(DateUtil.formatDate(item.getDateRealization()));
    }


    private void setImageStatus(TasksListAdapter.ViewHolder holder, Task item) {
        if (taskList != null) {
            switch (item.getStatus()) {
                case "Em andamento":
                    Picasso.with(context).load(R.color.colorPrimary).placeholder(R.color.colorPrimary).fit().into(holder.statusView);
                    Picasso.with(context).load(R.drawable.ic_done).fit().centerInside().into(holder.statusTask);
                    holder.statusTask.setClickable(true);
                    break;
                case "Finalizada":
                    Picasso.with(context).load(R.color.green_chart).placeholder(R.color.green_chart).fit().into(holder.statusView);
                    Picasso.with(context).load(R.drawable.ic_done_all).fit().centerInside().into(holder.statusTask);
                    holder.statusTask.setClickable(false);
                    break;
                case "Vencida":
                    Picasso.with(context).load(R.color.colorRedHumor).placeholder(R.color.colorRedHumor).fit().into(holder.statusView);
                    Picasso.with(context).load(R.drawable.ic_clock).fit().centerInside().into(holder.statusTask);
                    holder.statusTask.setClickable(true);
                    break;
            }
        }
    }

    private void setDescription(TasksListAdapter.ViewHolder holder, Task item) {
        if (taskList != null) {
            if (item.getDescription() != null)
                holder.titleTask.setText(item.getDescription());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return taskList.size() > 0 ? taskList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView excludeTask, statusTask,statusView;
        TextView titleTask, dateTask;

        ViewHolder(final View itemView) {
            super(itemView);

            excludeTask = (ImageView) itemView.findViewById(R.id.excludeTask);
            statusTask = (ImageView) itemView.findViewById(R.id.statusTask);
            statusView = (ImageView) itemView.findViewById(R.id.viewStatus);
            titleTask = (TextView) itemView.findViewById(R.id.titleTask);
            dateTask = (TextView) itemView.findViewById(R.id.dateTask);

            excludeTask.setOnClickListener(this);
            statusTask.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }

}



