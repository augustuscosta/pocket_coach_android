package br.com.otgmoblie.pocketcoach.views.task;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.service.TaskService;
import br.com.otgmoblie.pocketcoach.util.DateUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.util.ValidateUtil;
import br.com.otgmoblie.pocketcoach.views.goals.GoalActivity_;
import dmax.dialog.SpotsDialog;

@EActivity(R.layout.activity_new_task)
public class NewTaskActivity extends AppCompatActivity {

    @ViewById(R.id.toolbarNewTask)
    Toolbar toolbar;

    @ViewById(R.id.goalDescription)
    TextView goalDescription;

    @ViewById(R.id.goalDate)
    TextView goalDate;

    @ViewById(R.id.taskDescription)
    EditText taskDescription;

    @ViewById(R.id.frequencyArray)
    Spinner frequencyArray;

    @ViewById(R.id.stringDay)
    TextView textDay;

    @ViewById(R.id.taskDay)
    EditText taskDay;

    @ViewById(R.id.taskHour)
    EditText taskHour;

    @ViewById(R.id.stringDate)
    TextView textDate;

    @ViewById(R.id.taskDate)
    EditText taskDate;

    @ViewById(R.id.layoutDaysOfWeek)
    GridLayout layoutDaysOfWeek;

    @ViewById(R.id.sunday)
    Switch sunday;

    @ViewById(R.id.monday)
    Switch monday;

    @ViewById(R.id.tuesday)
    Switch tuesday;

    @ViewById(R.id.wednesday)
    Switch wednesday;

    @ViewById(R.id.thursday)
    Switch thursday;

    @ViewById(R.id.friday)
    Switch friday;

    @ViewById(R.id.saturday)
    Switch saturday;

    @ViewById(R.id.allDays)
    Switch allDays;

    @Bean
    TaskService taskService;

    @Extra("goalId")
    Integer idGoal;

    @Extra("goalDescription")
    String goalTitle;

    @Extra("goalDateRealization")
    String goalDateRealization;

    private String frequencyChosen;

    private SpotsDialog progress;


    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        goalDescription.setText(goalTitle);
        goalDate.setText(DateUtil.transformDate(goalDateRealization));

        createAdapter();
    }

    void createAdapter() {
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.frequency_array, android.R.layout.simple_spinner_item);
        frequencyArray.setAdapter(adapter);
    }

    @ItemSelect(R.id.frequencyArray)
    void myListItemSelected(boolean selected, String selectedItem) {
        frequencyChosen = selectedItem;
        switch (selectedItem) {
            case "Uma vez":
                hideDay();
                layoutDaysOfWeek.setVisibility(View.GONE);
                showDate();
                break;

            case "Semanal":
                hideDate();
                hideDay();
                layoutDaysOfWeek.setVisibility(View.VISIBLE);
                break;

            case "Mensal":
                hideDate();
                layoutDaysOfWeek.setVisibility(View.GONE);
                showDay();
                break;
        }
    }

    void showDate() {
        textDate.setVisibility(View.VISIBLE);
        taskDate.setVisibility(View.VISIBLE);
    }

    void hideDate() {
        textDate.setVisibility(View.GONE);
        taskDate.setVisibility(View.GONE);
    }

    void showDay() {
        textDay.setVisibility(View.VISIBLE);
        taskDay.setVisibility(View.VISIBLE);
    }

    void hideDay() {
        textDay.setVisibility(View.GONE);
        taskDay.setVisibility(View.GONE);
    }

    @Click(R.id.buttonCreateTask)
    void validateFields() {
        List<Boolean> checkedList = new ArrayList<>();

        checkedList.add(ValidateUtil.checkInterval(taskDescription, 1, 100, getString(R.string.invalid_description)));
        checkedList.add(ValidateUtil.checkTime(taskHour, getString(R.string.invalid_time)));

        switch (frequencyChosen) {

            case "Uma vez":
                checkedList.add(ValidateUtil.checkDate(taskDate, 10, getString(R.string.invalid_date)));
                break;

            case "Semanal":
                List<Boolean> boolArray = new ArrayList<>();
                boolArray.add(sunday.isChecked());
                boolArray.add(monday.isChecked());
                boolArray.add(tuesday.isChecked());
                boolArray.add(wednesday.isChecked());
                boolArray.add(thursday.isChecked());
                boolArray.add(friday.isChecked());
                boolArray.add(saturday.isChecked());
                checkedList.add(ValidateUtil.checkIfAllAreFalse(boolArray, new View(this)));
                break;

            case "Mensal":
                checkedList.add(ValidateUtil.checkDay(taskDay, getString(R.string.invalid_day)));
                break;
        }

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            createTask();
        }
    }

    @SuppressLint("SimpleDateFormat")
    void createTask() {
        Task task = new Task();

        task.setGoalId(idGoal);
        task.setDescription(taskDescription.getText().toString());
        task.setTypeFrequency(frequencyChosen);
        task.setStatus("Em andamento");

        setDate(task);
        setDay(task);
        setWeekDays(task);

        create(task);
    }

    @Background
    void create(Task task) {

        startProgress();

        if (taskService.create(task)) {
            closeProgress();
            showToast("Tarefa criada com sucesso!");
            callGoalActivity();
        } else {
            closeProgress();
            showToast("Erro ao criar a tarefa!");
        }
    }

    @UiThread
    void callGoalActivity() {
        GoalActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .flags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .extra("id", idGoal).start();
        finish();
    }

    @UiThread
    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void setDate(Task task) {
        if (taskDate.getVisibility() == View.VISIBLE) {
            task.setDateRealization(DateUtil.formatString(taskDate.getText().toString(), taskHour.getText().toString(), 1));
        } else {
            task.setDateRealization(DateUtil.formatString(goalDateRealization, taskHour.getText().toString(), 0));
        }
    }

    private void setDay(Task task) {
        if (taskDay.getVisibility() == View.VISIBLE)
            task.setDayOfMonth(Integer.parseInt(taskDay.getText().toString()));
    }

    private void setWeekDays(Task task) {
        if (layoutDaysOfWeek.getVisibility() == View.VISIBLE) {
            List<String> weekDays = new ArrayList<>();

            if (sunday.isChecked())
                weekDays.add("0");

            if (monday.isChecked())
                weekDays.add("1");

            if (tuesday.isChecked())
                weekDays.add("2");

            if (wednesday.isChecked())
                weekDays.add("3");

            if (thursday.isChecked())
                weekDays.add("4");

            if (friday.isChecked())
                weekDays.add("5");

            if (saturday.isChecked())
                weekDays.add("6");

            task.setWeekDays(weekDays);
        }
    }

    @CheckedChange(R.id.allDays)
    void changeAllDays(boolean isChecked) {
        if (isChecked) {
            sunday.setChecked(true);
            monday.setChecked(true);
            tuesday.setChecked(true);
            wednesday.setChecked(true);
            thursday.setChecked(true);
            friday.setChecked(true);
            saturday.setChecked(true);
        } else {
            sunday.setChecked(false);
            monday.setChecked(false);
            tuesday.setChecked(false);
            wednesday.setChecked(false);
            thursday.setChecked(false);
            friday.setChecked(false);
            saturday.setChecked(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @UiThread
    void startProgress() {
        progress = ProgressDialogUtil.callProgressDialog(this);
    }

    @UiThread
    void closeProgress() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

}
