package br.com.otgmoblie.pocketcoach.service;


import br.com.otgmoblie.pocketcoach.model.User;

public interface FacebookServiceCallback {
    void onFacebookLoginSuccess(User user);
    void onFacebookLoginError(String message);
}
