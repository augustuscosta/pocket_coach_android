package br.com.otgmoblie.pocketcoach.views.actions_plan;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.wefika.calendar.CollapseCalendarView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.List;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.service.TaskService;
import br.com.otgmoblie.pocketcoach.util.DialogUtil;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.task.TasksListAdapter;
import dmax.dialog.SpotsDialog;

/**
 * Created by thialyson on 17/04/17.
 */

@EFragment(R.layout.fragment_actions_plan)
public class ActionsPlanFragment extends Fragment implements AdapterView.OnItemClickListener {

    @ViewById(R.id.calendarView)
    CollapseCalendarView calendarView;

    @ViewById(R.id.recyclerViewActionsPlan)
    RecyclerView recyclerView;

    @ViewById(R.id.swipeRefreshActionsPlan)
    SwipeRefreshLayout swipeRefresh;

    @ViewById(R.id.tvNoTaskFound)
    TextView tvNoTaskFound;


    @Bean
    TaskService taskService;

    TasksListAdapter adapter;

    private List<Task> tasks;

    private SpotsDialog progress;

    @AfterViews
    void afterViews() {
        requestTasks();
        listenRefresh();
        listenCalendarChange();
    }

    @Background
    void requestTasks() {
        startProgress();
        try {
            taskService.requestAll();
            tasks = taskService.getAllByDate(new Date());
            closeProgress();
            setAdapter();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @UiThread
    void setAdapter() {
        if (tasks.size() > 0) {
            showRecyclerView();
            adapter = new TasksListAdapter(tasks, getActivity(), this);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            showTvNoTaskFound();
        }
    }

    void showRecyclerView() {
        tvNoTaskFound.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    void showTvNoTaskFound() {
        recyclerView.setVisibility(View.GONE);
        tvNoTaskFound.setVisibility(View.VISIBLE);
    }

    @UiThread
    void listenCalendarChange() {
        if (calendarView != null) {
            calendarView.init(LocalDate.now(), LocalDate.now(), LocalDate.now().plusYears(1));
            calendarView.setListener(new CollapseCalendarView.OnDateSelect() {
                @Override
                public void onDateSelected(LocalDate localDate) {
                    populateList(localDate);
                }
            });
        }
    }

    void populateList(LocalDate localDate) {
        tasks.clear();
        tasks = taskService.getAllByDate(localDate.toDate());

        if (tasks != null && tasks.size() > 0) {
            tvNoTaskFound.setVisibility(View.GONE);
            setAdapter();
        } else {
            setAdapter();
            tvNoTaskFound.setVisibility(View.VISIBLE);
        }
    }

    @Background
    void listenRefresh() {
        if (swipeRefresh != null) {
            swipeRefresh.setColorSchemeResources(R.color.com_facebook_blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {
                    requestTasks();
                    swipeRefresh.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Integer idTaskClicked = tasks.get(position).getId();

        switch (view.getId()) {
            case R.id.excludeTask:
                dialogAction(R.string.do_you_want_to_exclude_task, R.id.excludeTask, idTaskClicked);
                break;
            case R.id.statusTask:
                dialogAction(R.string.do_you_want_to_finalize_task, R.id.statusTask, idTaskClicked);
                break;
        }
    }

    @UiThread
    void dialogAction(int resIdQuestion, final int resIdViewId, final Integer idTask) {
        AlertDialog.Builder builder = DialogUtil.createDialog(getActivity(), getString(resIdQuestion));
        builder = DialogUtil.getNegativeButton(builder).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (resIdViewId) {
                    case R.id.excludeTask:
                        deleteTask(idTask);
                        break;
                    case R.id.statusTask:
                        finalizeTask(idTask);
                        break;
                }
            }
        });
        builder.show();
    }

    @Background
    void finalizeTask(Integer id) {
        startProgress();
        try {
            taskService.finalizeTask(id);
            closeProgress();
            requestTasks();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @Background
    void deleteTask(Integer id) {
        startProgress();
        try {
            taskService.deleteTask(id);
            closeProgress();
            requestTasks();
        } catch (Exception e) {
            closeProgress();
            e.printStackTrace();
        }
    }

    @UiThread
    void startProgress() {
        if (getActivity() != null)
            progress = ProgressDialogUtil.callProgressDialog(getActivity());
    }

    @UiThread
    void closeProgress() {
        if (getActivity() != null)
            ProgressDialogUtil.closeProgressDialog(progress);
    }

}
