package br.com.otgmoblie.pocketcoach.cloud;



import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import br.com.otgmoblie.pocketcoach.model.DeviceId;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Thialyson on 08/12/16.
 */
@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface WheelOfLifeRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("/wheels.json")
    List<WheelOfLive> getAll();

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Post("new_wheel_of_life")
    WheelOfLive send(@Body WheelOfLive wheelOfLive);

    String getCookie(String name);

    void setCookie(String name, String value);
}
