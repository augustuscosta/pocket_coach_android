package br.com.otgmoblie.pocketcoach.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.HumorRest;
import br.com.otgmoblie.pocketcoach.model.Humor;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by thialyson on 15/05/17.
 */
@EBean
public class HumorService {

    @RootContext
    Context context;

    @RestService
    HumorRest humorRest;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Humor, Integer> humorDao;

    @Bean(CookieService.class)
    CookieService cookieUtils;


    public Humor createHumor(Humor humor) throws Exception {

        Humor toReturn = null;
        try {
            setCookie();
            toReturn = humorRest.createHumor(humor);
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<Humor> requestAll() throws Exception {

        List<Humor> toReturn = null;
        try {
            setCookie();
            toReturn = humorRest.getAll();
            clearTable();
            persist(toReturn);
            saveCookie();
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    private void setCookie() {
        humorRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
    }

    private void saveCookie() {
        cookieUtils.storedCookie(humorRest.getCookie(RestUtil.MOBILE_COOKIE));
    }

    public List<Humor> getAll() {
        try {
            return humorDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Humor getById(Integer id) {
        try {
            return humorDao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Humor> getByDate(Date date) {
        try {
            List<Humor> teste = humorDao.queryBuilder().where().ge("date", date).query();
            return teste;
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private void persist(Humor humor) throws SQLException {
        if (humor == null)
            return;
        humorDao.createOrUpdate(humor);
    }


    public void persist(List<Humor> humors) throws SQLException {
        if (humors == null)
            return;
        for (Humor humor : humors) {
            try {
                persist(humor);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void clearTable() {
        try {
            TableUtils.clearTable(humorDao.getConnectionSource(), Humor.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
