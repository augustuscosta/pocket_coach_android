package br.com.otgmoblie.pocketcoach.views.temperament.affective;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.service.TemperamentService;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_find_solution)
public class FindSolutionFragment extends Fragment {

    @ViewById(R.id.btRedDarkAffective)
    Button buttonRedDark;

    @ViewById(R.id.btRedMediumAffective)
    Button buttonRedMedium;

    @ViewById(R.id.btRedLightAffective)
    Button buttonRedLight;

    @ViewById(R.id.btNeutralAffective)
    Button buttonWhite;

    @ViewById(R.id.btBlueDarkAffective)
    Button buttonBlueDark;

    @ViewById(R.id.btBlueMediumAffective)
    Button buttonBlueMedium;

    @ViewById(R.id.btBlueLightAffective)
    Button buttonBlueLight;

    @ViewById(R.id.textLeftAffective)
    TextView textLeft;

    @ViewById(R.id.textRightAffective)
    TextView textRight;


    Temperament temperament;

    @Bean
    TemperamentService temperamentService;

    @AfterViews
    void afterViews() {
        textLeft.setText(R.string.hard_find_solution);
        textRight.setText(R.string.easy_find_solution);

        temperament = temperamentService.getById(0);
        if (temperament != null) {
            checkItem();
        }
    }

    void checkItem() {
        if (temperament.getFindSolution() != null) {

            switch (temperament.getFindSolution()) {
                case 1:
                    changeValues(buttonRedDark, 1);
                    break;
                case 2:
                    changeValues(buttonRedMedium, 2);
                    break;
                case 3:
                    changeValues(buttonRedLight, 3);
                    break;
                case 4:
                    changeValues(buttonWhite, 4);
                    break;
                case 5:
                    changeValues(buttonBlueLight, 5);
                    break;
                case 6:
                    changeValues(buttonBlueMedium, 6);
                    break;
                case 7:
                    changeValues(buttonBlueDark, 7);
                    break;
            }
        } else {
            changeValues(buttonWhite, 4);
        }
    }

    @Click({R.id.btRedDarkAffective, R.id.btRedMediumAffective, R.id.btRedLightAffective, R.id.btNeutralAffective, R.id.btBlueDarkAffective, R.id.btBlueMediumAffective, R.id.btBlueLightAffective})
    void handleButtons(Button clickedButton) {

        switch (clickedButton.getId()) {
            case R.id.btRedDarkAffective:
                changeValues(buttonRedDark, 1);
                break;
            case R.id.btRedMediumAffective:
                changeValues(buttonRedMedium, 2);
                break;
            case R.id.btRedLightAffective:
                changeValues(buttonRedLight, 3);
                break;
            case R.id.btNeutralAffective:
                changeValues(buttonWhite, 4);
                break;
            case  R.id.btBlueLightAffective:
                changeValues(buttonBlueLight, 5);
                break;
            case R.id.btBlueMediumAffective:
                changeValues(buttonBlueMedium, 6);
                break;
            case R.id.btBlueDarkAffective:
                changeValues(buttonBlueDark, 7);
                break;
        }
    }

    void changeValues(Button button, Integer value) {
        clearButtons();
        button.setText(R.string.checkmark);
        temperament.setFindSolution(value);
    }

    @Click(R.id.btNext)
    void nextFragment() {
        if (temperament.getFindSolution() != null) {
            saveValue();
            switchFragment(new LearnMyErrorFragment_());
        }
    }

    @Click(R.id.btBack)
    void previousFragment() {
        if (temperament.getFindSolution() != null)
            saveValue();

        switchFragment(new ResolveConflictFragment_());
    }

    void saveValue() {
        if (temperament != null) {
            try {
                temperamentService.persist(temperament);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void clearButtons() {
        buttonWhite.setText("");
        buttonRedDark.setText("");
        buttonRedMedium.setText("");
        buttonRedLight.setText("");
        buttonBlueDark.setText("");
        buttonBlueMedium.setText("");
        buttonBlueLight.setText("");
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.temperament_container, fragment);
        transaction.commit();
    }
}
