package br.com.otgmoblie.pocketcoach.service;


import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.List;

import br.com.otgmoblie.pocketcoach.cloud.SessionRest;
import br.com.otgmoblie.pocketcoach.model.Experience;
import br.com.otgmoblie.pocketcoach.model.Goal;
import br.com.otgmoblie.pocketcoach.model.Humor;
import br.com.otgmoblie.pocketcoach.model.Session;
import br.com.otgmoblie.pocketcoach.model.Task;
import br.com.otgmoblie.pocketcoach.model.Temperament;
import br.com.otgmoblie.pocketcoach.model.User;
import br.com.otgmoblie.pocketcoach.model.UserWrapper;
import br.com.otgmoblie.pocketcoach.model.WheelOfLive;
import br.com.otgmoblie.pocketcoach.util.DatabaseHelper;
import br.com.otgmoblie.pocketcoach.util.RestUtil;

/**
 * Created by Geovanna on 08/12/16.
 */
@EBean
public class SessionService {

    @RestService
    SessionRest sessionRest;

    @RootContext
    Context context;

    @Bean(CookieService.class)
    CookieService cookieUtils;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Session, Integer> sessionDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Experience, Integer> experienceDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Goal, Integer> goalDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Humor, Integer> humorDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Task, Integer> taskDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Temperament, Integer> temperamentDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<WheelOfLive, Integer> wheelOfLiveDao;


    public boolean signIn(Session session) throws Exception {
        boolean returnValue;
        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            Session tesye = sessionRest.signIn(session);
            persist(tesye);
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    public boolean signInFacebook(User user) {
        boolean returnValue;
        try {

            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            UserWrapper wrraper = new UserWrapper();
            wrraper.setUser(user);
            persist(sessionRest.signInFacebook(wrraper));
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }


    public boolean signOut() {

        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.signOut();
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));
            clearTables();

            return true;

        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }

    }

    public boolean resetPassword(Session session) {
        boolean returnValue;
        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.resetPassword(session);
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    private void persist(Session session) {
        try {
            clearTable();
            sessionDao.createOrUpdate(session);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Session getCurrentSession() {
        try {
            List<Session> sessions = sessionDao.queryForAll();
            if (sessions.size() > 0)
                return sessions.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void clearTable() {
        try {
            TableUtils.clearTable(sessionDao.getConnectionSource(), Session.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clearTables() {
        try {
            TableUtils.clearTable(sessionDao.getConnectionSource(), Session.class);
            TableUtils.clearTable(userDao.getConnectionSource(), User.class);
            TableUtils.clearTable(experienceDao.getConnectionSource(), Experience.class);
            TableUtils.clearTable(humorDao.getConnectionSource(), Humor.class);
            TableUtils.clearTable(taskDao.getConnectionSource(), Task.class);
            TableUtils.clearTable(goalDao.getConnectionSource(), Goal.class);
            TableUtils.clearTable(temperamentDao.getConnectionSource(), Temperament.class);
            TableUtils.clearTable(wheelOfLiveDao.getConnectionSource(), WheelOfLive.class);

            FacebookService.saveSharedPreferencesTokenFacebook("", context);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
