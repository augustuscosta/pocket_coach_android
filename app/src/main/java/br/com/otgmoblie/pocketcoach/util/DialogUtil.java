package br.com.otgmoblie.pocketcoach.util;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

import com.squareup.picasso.Picasso;

import br.com.otgmoblie.pocketcoach.R;

public class DialogUtil {

    public static int valuePicker;

    public static void createDialogFacebookSemEmail(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String texto = context.getString(R.string.error_facebook_sem_email);
        builder.setMessage(texto);
        builder.setNegativeButton(R.string.ok, getClickListener());
        builder.create().show();
    }

    public static AlertDialog.Builder createDialog(Context context, String message) {
        return new AlertDialog.Builder(context, R.style.MyDialogTheme)
                .setCancelable(false)
                .setIcon(R.mipmap.ic_app)
                .setTitle(R.string.app_name)
                .setMessage(message);
    }

    public static AlertDialog.Builder getNegativeButton(AlertDialog.Builder builder) {
        builder.setNegativeButton(R.string.cancel, getClickListener());
        return builder;
    }

    public static AlertDialog.Builder getPositiveButton(AlertDialog.Builder builder) {
        builder.setPositiveButton(R.string.ok, getClickListener());
        return builder;
    }

    private static DialogInterface.OnClickListener getClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
    }

    @SuppressLint("NewApi")
    public static View getNumberPicker(Context context) {
        final NumberPicker picker = new NumberPicker(context);
        final FrameLayout layout = new FrameLayout(context);

        layout.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        picker.setMinValue(0);
        picker.setMaxValue(10);
        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                valuePicker = newVal;
            }
        });

        return layout;
    }


}
