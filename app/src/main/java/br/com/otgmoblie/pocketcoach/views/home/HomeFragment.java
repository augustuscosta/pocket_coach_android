package br.com.otgmoblie.pocketcoach.views.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmoblie.pocketcoach.R;
import br.com.otgmoblie.pocketcoach.util.ProgressDialogUtil;
import br.com.otgmoblie.pocketcoach.views.actions_plan.ActionsPlanFragment_;
import br.com.otgmoblie.pocketcoach.views.goals.GoalsGridFragment;
import br.com.otgmoblie.pocketcoach.views.goals.GoalsGridFragment_;
import dmax.dialog.SpotsDialog;

@EFragment(R.layout.fragment_home)
public class HomeFragment extends Fragment {

    @ViewById(R.id.ivActionsPlan)
    ImageView imageActionsPlan;

    @ViewById(R.id.ivWantToGo)
    ImageView imageWantToGo;

    @ViewById(R.id.ivImNow)
    ImageView imageImNow;

    @AfterViews
    void afterViews(){
        Picasso.with(getActivity()).load(R.drawable.clipboard).fit().centerInside().into(imageActionsPlan);
        Picasso.with(getActivity()).load(R.drawable.trending).fit().centerInside().into(imageWantToGo);
        Picasso.with(getActivity()).load(R.drawable.now).fit().centerInside().into(imageImNow);
    }

    @Click(R.id.cardActionsPlan)
    void clickActionsPlan(){
        switchFragment(new ActionsPlanFragment_());
    }

    @Click(R.id.cardWantToGo)
    void clickWantToGo(){
        goToGoalsGridFragment(0);
    }

    @Click(R.id.cardImNow)
    void clickImNow(){
        goToGoalsGridFragment(1);
    }

    void goToGoalsGridFragment(int key){

        Fragment fragment = new GoalsGridFragment_();
        Bundle bundle = new Bundle();

        bundle.putInt("keyGrid",key);
        fragment.setArguments(bundle);
        switchFragment(fragment);
    }
    
    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

}
